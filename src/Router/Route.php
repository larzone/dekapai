<?php namespace Dekapai\Router;

use Dekapai\Exceptions\Nyuugan;

class Route
{
    private $requestMethod;
    private $uri;
    private $controller;
    private $method;
    private $middleware;
    private $parameters;
    private $name;

    private function __construct($requestMethod, $uri, $controllerAndMethod, array $middleware, $name)
    {
        if (is_array($controllerAndMethod)) {
            if (count($controllerAndMethod) != 2) {
                throw new Nyuugan("Controller and method must be an array with 2 elements");
            }
            $this->controller = $controllerAndMethod[0];
            $this->method = $controllerAndMethod[1];
        } else {
            $this->controller = $controllerAndMethod;
            $this->method = '__invoke';
        }
        $this->requestMethod = $requestMethod;
        $this->uri = trim($uri, '/');
        $this->middleware = $middleware;
        $this->name = $name;
    }

    public static function GET($uri, $controllerAndMethod, array $middleware = [], $name = '')
    {
        return new static('GET', $uri, $controllerAndMethod, $middleware, $name);
    }

    public static function POST($uri, $controllerAndMethod, array $middleware = [], $name = '')
    {
        return new static('POST', $uri, $controllerAndMethod, $middleware, $name);
    }

    public static function PUT($uri, $controllerAndMethod, array $middleware = [], $name = '')
    {
        return new static('PUT', $uri, $controllerAndMethod, $middleware, $name);
    }

    public static function DELETE($uri, $controllerAndMethod, array $middleware = [], $name = '')
    {
        return new static('DELETE', $uri, $controllerAndMethod, $middleware, $name);
    }

    public static function PATCH($uri, $controllerAndMethod, array $middleware = [], $name = '')
    {
        return new static('PATCH', $uri, $controllerAndMethod, $middleware, $name);
    }

    public static function NAMESPACE($uri, callable $closure, array $middleware)
    {
        return new NamaeKuukan($uri, $closure, $middleware);
    }

    public function getRequestMethod() { return $this->requestMethod; }
    public function getUri() { return $this->uri; }
    public function getController() { return $this->controller; }
    public function getMethod() { return $this->method; }
    public function getParameters() { return $this->parameters; }
    public function setParameters($parameters) { $this->parameters = $parameters; }
    public function getName() { return $this->name; }
    public function setUri($uri) { $this->uri = $uri; }
    public function getRouteMiddleware() { return $this->middleware; }
    public function pushRouteMiddleware($middleware) { $this->middleware[] = $middleware; }

}
