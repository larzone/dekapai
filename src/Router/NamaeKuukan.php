<?php namespace Dekapai\Router;

class NamaeKuukan
{
    public $uri;
    public $closure;
    public $middleware;

    public function __construct($uri, $closure, $middleware)
    {
        $this->uri = $uri;
        $this->closure = $closure;
        $this->middleware = $middleware;
    }
}
