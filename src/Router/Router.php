<?php namespace Dekapai\Router;

use Dekapai\Exceptions\HttpException;
use Dekapai\Exceptions\PageNotFoundException;

class Router
{
    /**
     * @var callable
     */
    private $routeCollection;
    private $uri;
    private $requestMethod;

    public function __construct(callable $routeCollection, $uri, $requestMethod)
    {
        $this->routeCollection = $routeCollection;
        $this->uri = trim($uri, '/');
        $this->requestMethod = $requestMethod;
    }

    public function resolveRoute()
    {
        if (strlen($this->uri) > 200)
            throw new HttpException("Request URI too long", 414);

        // First pass: check method (http verb) and return if a match is found (common case)
        $found = $this->processWrapper(true);
        if ($found !== false) return $found;
        
        // Second pass: don't check method. if there's a match with the wrong method, throw appropriate exception
        $found = $this->processWrapper(false);
        if ($found === false) {
            throw new PageNotFoundException($this->uri);
        }
        else {
            throw new HttpException("Method Not Allowed", 405);
        }
    }

    private function processWrapper($checkMethod) {
        $cb = $this->routeCollection;
        foreach ($cb() as $route) {
            $result = $this->processRoute($route, $checkMethod);
            if (false === $result) continue;
            return $result;
        }
        return false;
    }
    
    private function processRoute($route, $checkMethod, $prefixes = [], $extraMiddleware = [])
    {
        if ($route instanceof NamaeKuukan) {
            $ns = $route;
            $closure = $ns->closure;
            foreach ($closure() as $routeInNamespace) {
                $result = $this->processRoute($routeInNamespace, $checkMethod, array_merge($prefixes, [$ns->uri]), array_merge($extraMiddleware, $ns->middleware));
                if (false !== $result) {
                    return $result;
                }
            }
            return false;
        }

        if (!($route instanceof Route)) {
            throw new \Exception("Invalid route.");
        }

        if ($checkMethod) {
            $method = $route->getRequestMethod();
            if ($this->requestMethod !== $method) return false;
        }

        $uri = implode('/', array_filter(array_merge($prefixes, [$route->getUri()]), function($x) { return !empty($x); }));
        $regex = self::replacePlaceholders($uri);

        if (!preg_match($regex, $this->uri, $match)) {
            return false;
        }

        $namedParameters = self::getNamedParameters($uri);
        array_shift($match);
        $params = array();

        for ($i = 0; $i < count($match); $i++) {
            $key = $namedParameters[$i];
            $paramValue = $match[$i];
            if (!strlen($paramValue)) continue;

            if (strstr($key, '|')) {

                list($key, $modifier) = explode('|', $key);
                $parameterToModifier = null;
                if (strstr($modifier, '(')) {
                    preg_match('/(.*?)\((.*?)\)/', $modifier, $m);
                    $modifier = $m[1];
                    $parameterToModifier = $m[2];
                }
                switch ($modifier) {
                    case('int'):
                        if (!preg_match('/^\d+$/', $paramValue)) return false;
                        $paramValue = (int) $paramValue;
                        break;
                    case('date'):
                        if (!preg_match('/\d{4}-\d{2}-\d{2}/', $paramValue)) return false;
                        $date = new \DateTime($paramValue);
                        if ($date->format('Y-m-d') != $paramValue) return false;
                        break;
                    case('bool'):
                    case('boolean'):
                        $paramValue = strtolower($paramValue);
                        if (!in_array($paramValue, array('true', 'false'))) return false;
                        $paramValue = ($paramValue == 'true');
                        break;
                    case('name'):
                        $key = $parameterToModifier;
                        break;
                }
            }

            $params[$key] = $paramValue;

        }

        $route->setParameters($params);

        if (count($prefixes) > 0 || count($extraMiddleware) > 0) {
            $newRoute = clone $route;
            $newRoute->setUri($uri);
            foreach ($extraMiddleware as $mw) {
                $newRoute->pushRouteMiddleware($mw);
            }
            return $newRoute;
        }

        return $route;
    }

    public static function replacePlaceholders(string $uri)
    {
        $temp = str_replace('/', '\\/', $uri);
        $temp = str_replace('(', '(?i:', $temp);
        $regex = '/^' . preg_replace('/\{[^\/]+\}/', '([^\/]+?)', $temp) . '$/';
        return $regex;
    }

    public static function getNamedParameters(string $uri)
    {
        preg_match_all('/\{([^}]+)\}/', $uri, $matches);
        $namedParameters = $matches[1];
        return $namedParameters;
    }
}
