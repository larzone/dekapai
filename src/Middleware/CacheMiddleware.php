<?php namespace Dekapai\Middleware;

use Closure;
use Dekapai\Exceptions\KeyNotFoundException;
use Dekapai\ResponseCache\ResponseCache;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class CacheMiddleware implements Middleware
{
    private $uri;
    private $lifeTime;

    /**
     * Determines which headers to include in the response cache (case-insensitive).
     *
     * @var array $headers
     */
    public static $headers = ['Content-Type', 'Content-Security-Policy'];

    private $cacheHandler;

    public function __construct(ResponseCache $cacheHandler, $cache = null)
    {
        $this->cacheHandler = $cacheHandler;
        $this->lifeTime = $cache;
    }

    private function sanitizeEtag($Etag)
    {
        return preg_replace('/[^a-f0-9-]/', '', $Etag);
    }

    public function __invoke(Request $request, Response $response, Closure $next)
    {
        if ((defined('DISABLE_CACHE') && DISABLE_CACHE) || is_null($this->lifeTime)) {
            return $next($request, $response);
        }
        $this->uri = $request->attributes->get('uri');
        try {
            // don't bother fetching the whole response if the e-tag is valid
            $clientEtag = $this->sanitizeEtag($request->headers->get("If-None-Match", "none"));
            if ($clientEtag != "none" && $this->cacheHandler->hasEtag($clientEtag)) {
                return new Response('', 304);
            }
            $cachedResponse = $this->cacheHandler->getCachedResponse($this->uri);
            // check last modified
            $ifModifiedSince = $request->headers->get("If-Modified-Since", "none");
            if ($ifModifiedSince != "none") {
                $ifModifiedSince = intval(strtotime($ifModifiedSince));
                if ($ifModifiedSince != 0) {
                    $expires = intval(strtotime($cachedResponse->headers->get("Expires")));
                    if ($expires > $ifModifiedSince) {
                        return new Response('', 304);
                    }
                }
            }
            return $cachedResponse;
        } catch (KeyNotFoundException $e) {
            $response = $next($request, $response);
            $this->cacheHandler->saveResponse($this->uri, $response, $this->lifeTime, static::$headers);
            return $response;
        }
    }
}
