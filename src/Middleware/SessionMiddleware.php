<?php namespace Dekapai\Middleware;

use Closure;
use Dekapai\Config\Config;
use Dekapai\Session;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class SessionMiddleware implements Middleware
{

    /**
     * @var Session
     */
    private $session;

    public function __construct(Session $session, Config $config)
    {
        $this->session = $session;
        $session->setNamespace($config->get("app.session.namespace"));
    }

    public function __invoke(Request $request, Response $response, Closure $next)
    {
        $this->session->start();
        $request->setSession($this->session);
        $response = $next($request, $response);
        $this->session->finalize($response);
        return $response;
    }
}
