<?php namespace Dekapai\Middleware;

use Closure;
use Dekapai\Security\CSP;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class CspMiddleware implements Middleware
{
    private $csp;

    public function __construct(CSP $csp)
    {
        $this->csp = $csp;
    }

    public function __invoke(Request $request, Response $response, Closure $next)
    {
        $response = $next($request, $response);
        $response->headers->set('Content-Security-Policy', (string) $this->csp);
    }
}
