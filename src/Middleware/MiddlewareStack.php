<?php namespace Dekapai\Middleware;

use Closure;
use Dekapai\Exceptions\HttpException;
use function Dekapai\Hinnyuu\Facade\hinnyuu;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class MiddlewareStack
{
    private $pq;

    public function __construct()
    {
        $this->pq = new \SplPriorityQueue();
    }

    public function push($middleware, $priority = 0)
    {
        $this->pq->insert($middleware, $priority);
    }

    public function execute(Request $request, Response $response, array $decorators, Closure $controller)
    {
        $localPq = clone $this->pq; // for multiple dispatch
        $next = function(Request $request, Response $response, $nxt) use ($decorators, $localPq, $controller) {
            if ($localPq->isEmpty()) {
                $response = $controller();
            } else {
                $className = $localPq->extract();
                $middleware = $this->getInstance($className, $decorators);
                $response = $middleware($request, $response, function ($request, $response) use ($nxt) {
                    return $nxt($request, $response, $nxt);
                });
            }
            return $response;
        };
        $response = $next($request, $response, $next);
        return $response;
    }

    /**
     * @param $className
     * @param array $decorators
     * @return Middleware
     * @throws HttpException
     */
    private function getInstance($className, array $decorators)
    {
        $instance = hinnyuu($className, $decorators);
        if (!($instance instanceof Middleware)) throw new HttpException("Expectation failed.", 412);
        return $instance;
    }
}
