<?php namespace Dekapai\Security;

class CSP
{

    const NONE = 0;
    const SELF = 1;
    const UNSAFE_INLINE = 2;
    const UNSAFE_EVAL = 3;

    // valid values for referrer
    const NO_REFERRER = 4;
    const NO_REFERRER_WHEN_DOWNGRADE = 5;
    const ORIGIN = 6;
    const ORIGIN_WHEN_CROSS_ORIGIN = 7;
    const UNSAFE_URL = 8;

    // valid values for reflected-xss
    const ALLOW = 9;
    const BLOCK = 10;
    const FILTER = 11;

    private $policy = [];

    private function addPolicy(array $a, $c)
    {
        if (!isset($this->policy[$c])) {
            $this->policy[$c] = [];
        }
        foreach ($a as $b) {
            $bb = $b;
            if ($b === self::NONE) $bb = "'none'";
            if ($b === self::SELF) $bb = "'self'";
            if ($b === self::UNSAFE_INLINE) $bb = "'unsafe-inline'";
            if ($b === self::UNSAFE_EVAL) $bb = "'unsafe-eval'";
            if (!is_string($bb)) {
                throw new \Exception("Invalid value for {$c} in content security policy.");
            }
            if (!in_array($bb, $this->policy[$c])) array_push($this->policy[$c], $bb);
        }
        return $this;
    }

    private function setReferrer($s)
    {
        $ss = "";
        if ($s == self::NO_REFERRER)                $ss = "no-referrer";
        if ($s == self::NO_REFERRER_WHEN_DOWNGRADE) $ss = "no-referrer-when-downgrade";
        if ($s == self::ORIGIN)                     $ss = "origin";
        if ($s == self::ORIGIN_WHEN_CROSS_ORIGIN)   $ss = "origin-when-cross-origin";
        if ($s == self::UNSAFE_URL)                 $ss = "unsafe-url";
        if (in_array(["no-referrer", "no-referrer-when-downgrade", "origin", "origin-when-cross-origin", "unsafe-url"], $s)) {
            $ss = $s;
        }
        if (strlen($ss) == 0) throw new \Exception("Invalid value for referrer in content security policy.");
        $this->policy['referrer'] = [$ss];
        return $this;
    }

    private function setReflectedXss($s)
    {
        $ss = "";
        if ($s == self::ALLOW)  $ss = "allow";
        if ($s == self::BLOCK)  $ss = "block";
        if ($s == self::FILTER) $ss = "filter";
        if (in_array(["allow", "block", "filter"], $s)) {
            $ss = $s;
        }
        if (strlen($ss) == 0) throw new \Exception("Invalid value for reflected-xss in content security policy.");
        $this->policy['reflected-xss'] = [$ss];
        return $this;
    }

    private function setScalar($s, $key)
    {
        if (!is_string($s)) throw new \Exception("Invalid value for {$key} in content security policy.");
        $this->policy[$key] = [$s];
        return $this;
    }

    // Accepts an array [ 'base-uri' => [ "'self'" ], 'font-src' => [ "'self'", 'http://cdn.com' ] ... ]
    // Useful for when the CSP is stored in a json file or similar
    public static function fromArray(array $a)
    {
        $obj = new static();
        foreach ($a as $key => $val) {
            if ($key == 'referrer') {
                $obj->setReferrer($val);
            } else if ($key == 'reflected-xss') {
                $obj->setReflectedXss($val);
            } else if ($key == 'report-uri' || $key == 'sandbox') {
                $obj->setScalar($val, $key);
            } else if ($key == 'upgrade-insecure-requests') {
                $obj->setScalar('', $key);
            } else {
                $obj->addPolicy($val, $key);
            }
        }
        return $obj;
    }

    public function baseUri(array $a) { return $this->addPolicy($a, 'base-uri'); }
    public function childSrc(array $a) { return $this->addPolicy($a, 'child-src'); }
    public function connectSrc(array $a) { return $this->addPolicy($a, 'connect-src'); }
    public function defaultSrc(array $a) { return $this->addPolicy($a, 'default-src'); }
    public function fontSrc(array $a) { return $this->addPolicy($a, 'font-src'); }
    public function formAction(array $a) { return $this->addPolicy($a, 'form-action'); }
    public function frameAncestors(array $a) { return $this->addPolicy($a, 'frame-ancestors'); }
    /** @deprecated */
    public function frameSrc(array $a) { return $this->addPolicy($a, 'frame-src'); }
    public function imgSrc(array $a) { return $this->addPolicy($a, 'img-src'); }
    public function manifestSrc(array $a) { return $this->addPolicy($a, 'manifest-src'); }
    public function mediaSrc(array $a) { return $this->addPolicy($a, 'media-src'); }
    public function objectSrc(array $a) { return $this->addPolicy($a, 'object-src'); }
    public function pluginTypes(array $a) { return $this->addPolicy($a, 'plugin-types'); }
    public function referrer($s) { return $this->setReferrer($s); }
    public function reflectedXss($s) { return $this->setReflectedXss($s); }
    public function reportUri($s) { return $this->setScalar($s, 'report-uri'); }
    public function sandbox($s) { return $this->setScalar($s, 'sandbox'); }
    public function scriptSrc(array $a) { return $this->addPolicy($a, 'script-src'); }
    public function styleSrc(array $a) { return $this->addPolicy($a, 'style-src'); }
    public function upgradeInsecureRequests() { return $this->setScalar('', 'upgrade-insecure-requests'); }

    public function __toString()
    {
        $arr = [];
        foreach ($this->policy as $key => $value) $arr[] = trim(sprintf("%s %s", $key, implode(' ', $value)));
        return implode('; ', $arr);
    }
}
