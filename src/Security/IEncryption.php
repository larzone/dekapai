<?php namespace Dekapai\Security;


interface IEncryption
{
    public function encrypt($plainText, $key, $salt);
    public function decrypt($cipherText, $key, $salt);
}
