<?php namespace Dekapai\Security;

class OpenSSL implements IEncryption
{
    const METHOD = 'aes-256-cbc';

    private function gen_enc_key($key)
    {
        return hash_hmac('sha256', $key, 'first salt', true);
    }

    private function gen_hmac_key($text, $salt)
    {
        return hash_hmac('sha256', $text, $salt, true);
    }

    private function createIV($ivSize)
    {
        if (function_exists('mcrypt_create_iv')) {
            $iv = mcrypt_create_iv($ivSize, MCRYPT_DEV_URANDOM);
        } elseif (function_exists('random_bytes')) {
            $iv = random_bytes($ivSize);
        } elseif (is_readable('/dev/urandom')) {
            $f = fopen('/dev/urandom', 'r');
            $iv = fread($f, $ivSize);
            fclose($f);
        } else {
            throw new \Exception("Unable to create IV.");
        }
        if (mb_strlen($iv,'8bit') != $ivSize) {
            throw new \Exception("Unable to create IV.");
        }
        return $iv;
    }

    public function encrypt($plainText, $key, $salt)
    {
        $secureKey = $this->gen_enc_key($key);
        $ivSize = openssl_cipher_iv_length(self::METHOD);
        $iv = $this->createIV($ivSize);
        $cipherText = openssl_encrypt($plainText, self::METHOD, $secureKey, OPENSSL_RAW_DATA, $iv);
        $hmacKey = $this->gen_hmac_key($iv . self::METHOD . $cipherText, $salt);
        return base64_encode($hmacKey . $iv . $cipherText);
    }

    public function decrypt($cipherText, $key, $salt)
    {
        $cipherText = base64_decode($cipherText);
        if ($cipherText === false) return false;

        $hmac       = mb_substr($cipherText, 0,  32,   '8bit');
        $iv         = mb_substr($cipherText, 32, 16,   '8bit');
        $cipherText = mb_substr($cipherText, 48, null, '8bit');

        if (hash_equals($hmac, $this->gen_hmac_key($iv . self::METHOD . $cipherText, $salt))) {
            return openssl_decrypt(  // returns false on fail
                $cipherText,
                self::METHOD,
                $this->gen_enc_key($key),
                OPENSSL_RAW_DATA,
                $iv
            );
        }
        return false;
    }
}
