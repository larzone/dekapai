<?php namespace Dekapai\Database;

use function Dekapai\Hinnyuu\Facade\hinnyuu;

class DB
{
    /**
     * @var MysqlDb
     */
    private static $instance = null;

    private static function getInstance()
    {
        if (is_null(static::$instance)) {
            //static::$instance = new MysqlDb();
            static::$instance = hinnyuu(MysqlDb::class);
        }
        return static::$instance;
    }

    // with this we don't need to specify every method, but it makes life harder for the IDE because it can't see the magic methods
    /*
    public static function __callStatic($name, $arguments)
    {
        $db = MysqlDbSingleton::getInstance();
        $reflectionMethod = new \ReflectionMethod('Dekapai\Database\MysqlDb', $name);
        return $reflectionMethod->invokeArgs($db, $arguments);
    }
    */

    /*
    private static function logQuery($sql)
    {
        if (Oppai::getEnv() == 'dev') {
            $f = fopen(tempdir() . 'querylog.sql', 'a+');
            fputs($f, $sql . "\n");
            fclose($f);
        }
    }
    */


    public static function prepare($sql)
    {
        //static::logQuery($sql);
        $dbc = self::getInstance();
        return $dbc->prepare($sql);
    }

    public static function quote($str)
    {
        $dbc = self::getInstance();
        return $dbc->quote($str);
    }

    public static function query($sql)
    {
        //static::logQuery($sql);
        $dbc = self::getInstance();
        return $dbc->query($sql);
    }

    public static function lastInsertId()
    {
        $dbc = self::getInstance();
        return $dbc->lastInsertId();
    }

    public static function beginTransaction()
    {
        $dbc = self::getInstance();
        return $dbc->beginTransaction();
    }

    public static function commit()
    {
        $dbc = self::getInstance();
        return $dbc->commit();
    }

    public static function rollBack()
    {
        $dbc = self::getInstance();
        return $dbc->rollBack();
    }

    // Returns the number of rows in $table that match $column = $testEqual
    public static function getRowCount($table, $column, $testEqual)
    {
        $dbc = self::getInstance();
        return $dbc->getRowCount($table, $column, $testEqual);
    }

    public static function errorInfo()
    {
        $dbc = self::getInstance();
        return $dbc->errorInfo();
    }
}
