<?php namespace Dekapai\Database;

use Dekapai\Config\DbConfig;
use Dekapai\Interfaces\IRowCountable;
use \PDO;
use PDOException;

class MysqlDb extends PDO implements IRowCountable
{
    private $disallowedChars = '[^a-zA-Z0-9_-]';

    public function __construct(DbConfig $config)
    {
        try {
            parent::__construct(
                sprintf(
                    "mysql:host=%s;dbname=%s",
                    $config->get('db.hostname'),
                    $config->get('db.database')
                ),
                $config->get('db.username'),
                $config->get('db.password')
            );
            $this->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
            $this->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (PDOException $e) {
            echo("Database error.");
            exit;
        }
    }

    // Returns the number of rows in $table that match $column = $testEqual
    public function getRowCount($table, $column, $testEqual)
    {
        $table = preg_replace($this->disallowedChars, '', $table);
        $column = preg_replace($this->disallowedChars, '', $column);
        $sth = $this->prepare("SELECT 1 FROM {$table} WHERE {$column} = :key");
        $sth->bindParam('key', $testEqual);
        $sth->execute();
        $rowCount = $sth->rowCount();
        $sth->closeCursor();
        return $rowCount;
    }
}
