<?php namespace Dekapai\ResponseCache;

use DateTime;
use Dekapai\Config\Config;
use Dekapai\Exceptions\KeyNotFoundException;
use Dekapai\Interfaces\IKeyValueStore;
use Symfony\Component\HttpFoundation\Response;

class ResponseCache
{

    private $store;
    private $salt;
    
    private function mkKeyAndEtag($uri)
    {
        $md5uri = substr(md5($uri), 0, 8);
        $key = "CachedResponse_" . $md5uri;
        $Etag = $md5uri . '-' . substr(md5($uri . microtime(true) . $this->salt), 0, 4);
        return [$key, $Etag];
    }

    public function __construct(IKeyValueStore $store, Config $config)
    {
        $this->store = $store;
        $this->salt = $config->get('app.security.salt');
    }

    /**
     * @param $uri string
     * @return Response
     * @throws KeyNotFoundException
     */
    public function getCachedResponse($uri)
    {
        list($key) = $this->mkKeyAndEtag($uri);
        $cachedResponse = $this->store->get($key);
        $response = new Response($cachedResponse['body'], 200);
        $headers = $cachedResponse['headers'];
        foreach ($headers as $key => $value) {
            $response->headers->set($key, $value);
        }
        $response->headers->set('Etag', $cachedResponse['Etag']);
        $response->setPublic();
        $response->setExpires(DateTime::createFromFormat('U', $cachedResponse['ExpiresTimestamp']));
        $response->setLastModified(DateTime::createFromFormat('U', $cachedResponse['LastModTimestamp']));
        return $response;
    }

    public function saveResponse($uri, Response $response, $lifeTime, array $headers)
    {
        $lifeTime = (int) $lifeTime;

        if ($lifeTime <= 0)
            throw new \Exception("Lifetime not set.");

        if ($response->getStatusCode() != 200) return;

        $headersArr = [];
        foreach ($headers as $header) {
            if ($response->headers->has($header)) {
                $headersArr[$header] = $response->headers->get($header);
            }
        }

        list($key, $Etag) = $this->mkKeyAndEtag($uri);

        $headersArr['Etag'] = $Etag;

        $cachedResponse = [
            'body' => $response->getContent(),
            'headers' => $headersArr,
            'Etag' => $Etag,
            'LastModTimestamp' => time(),
            'ExpiresTimestamp' => time() + $lifeTime
        ];
        $this->store->set($key, $cachedResponse, $lifeTime);
        $this->store->set('Etag_' . $Etag, true, $lifeTime);
    }

    public function invalidateCache($uri)
    {
        list($key, $Etag) = $this->mkKeyAndEtag($uri);
        $this->store->invalidate($key);
        $this->store->invalidate('Etag_' . $Etag);
    }

    public function hasEtag($clientEtag)
    {
        try {
            $this->store->get('Etag_' . $clientEtag);
            return true;
        } catch (\Exception $e) {
            return false;
        }
    }

}
