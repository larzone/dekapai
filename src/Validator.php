<?php namespace Dekapai;

use Dekapai\Exceptions\Nyuugan;

class Validator
{

    private $errors = [];
    private $plugins = [];

    public function validate(array $input)
    {
        $errors = array();
        foreach ($input as $key => $val) {
            $userInput = $val[0];
            $verifyStr = $val[1];
            $verifyArr = explode('|', $verifyStr);

            $allTokens = array();
            foreach ($verifyArr as $token) {
                if (preg_match('/(.+?)\((.+)\)$/', $token, $match)) {
                    $token = $match[1];
                }
                $allTokens[] = $token;
            }

            foreach ($verifyArr as $token) {
                $parameter = '';
                if (preg_match('/(.+?)\((.+)\)$/', $token, $match)) {
                    $token = $match[1];
                    $parameter = $match[2];
                }
                if (!in_array('required', $allTokens) && strlen($userInput) == 0) {
                    continue; // don't verify if nothing entered
                }
                switch ($token) {
                    case ('date'):
                        if (!preg_match('/\d{4}-\d{2}-\d{2}/', $userInput)) {
                            $errors[] = "Date in field {$key} must be formatted as YYYY-MM-DD.";
                            continue;
                        }
                        preg_match('/(\d{4})-(\d{2})-(\d{2})/', $userInput, $match);
                        $y = (int) $match[1];
                        $m = (int) $match[2];
                        $d = (int) $match[3];
                        if ($y < 1000 || $y > 3000) {
                            $errors[] = "Year in date must be between 1000 and 3000.";
                            continue;
                        }
                        if ($m < 1 || $m > 12) {
                            $errors[] = "Month in date must be between 1 and 12.";
                            continue;
                        }
                        if ($d < 1 || $d > 31) {
                            $errors[] = "Day in date must be between 1 and 31.";
                            continue;
                        }
                        if (date('Y-m-d', strtotime($userInput)) != $userInput) {
                            $errors[] = "The date is not valid (possibly day out of range for given month).";
                            continue;
                        }
                                         break;
                    case ('maxage'):
                        if (!in_array('date', $allTokens)) {
                            throw new Nyuugan("Filter maxage requires type to be date.");
                        }
                        if (!empty($userInput)) {
                            $maxage = (float) $parameter;
                            try {
                                $dob = new \DateTime($userInput);
                                $now = new \DateTime();
                                if ($now->diff($dob)->format('%Y') > $maxage) {
                                    $errors[] = "Age for {$key} cannot be greater than {$maxage}.";
                                    continue;
                                }
                            } catch (\Exception $e) {
                                $errors[] = "Invalid date format in field {$key}.";
                                continue;
                            }
                        }
                                         break;
                    case ('minage'):
                        if (!in_array('date', $allTokens)) {
                            throw new Nyuugan("Filter maxage requires type to be date.");
                        }
                        if (!empty($userInput)) {
                            $minage = (float) $parameter;
                            try {
                                $dob = new \DateTime($userInput);
                                $now = new \DateTime();
                                if ($now->diff($dob)->format('%Y') < $minage) {
                                    $errors[] = "Age for {$key} cannot be less than {$minage}.";
                                }
                            } catch (\Exception $e) {
                                $errors[] = "Invalid date format in field {$key}.";
                            }
                        }
                                         break;
                    case ('float'):
                        if (strlen($userInput) && !filter_var($userInput, FILTER_VALIDATE_FLOAT)) {
                            $errors[] = "Field {$key} must be a valid floating point number.";
                            continue;
                        }
                                         break;
                    case ('int'):
                        if (strlen($userInput) && !preg_match('/^-?\d+$/', $userInput)) {
                            $errors[] = "Field {$key} must be a valid integer.";
                            continue;
                        }
                                         break;
                    case ('required'):
                        if (strlen($userInput) == 0) {
                            $errors[] = "The field {$key} is required.";
                            continue;
                        }
                                         break;

                    case('in'):
                        if (strlen($parameter) == 0) {
                            throw new Nyuugan("Validator: IN modifier used without a parameter.");
                        }
                        $allowed = explode(',', $parameter);
                        if (!in_array($userInput, $allowed)) {
                            $errors[] = "The field {$key} must be one of: " . $parameter;
                            continue;
                        }
                                         break;
                    case ('max'):
                        if (strlen($parameter) == 0) {
                            throw new Nyuugan("Validator: max modifier used without a parameter.");
                        }
                        if (in_array('float', $allTokens) || in_array('int', $allTokens)) { // test as numeric
                            $val = (float) $userInput;
                            $maxval = (float) $parameter;
                            if ($val > $maxval) {
                                $errors[] = "The numeric field {$key} must be less than or equal to {$maxval}.";
                                continue;
                            }
                        } else { // test as string
                            if (strlen($userInput) > (int) $parameter) {
                                $errors[] = "The field {$key} must be shorter than {$parameter} characters.";
                                continue;
                            }
                        }
                                         break;
                    case ('min'):
                        if (strlen($parameter) == 0) {
                            throw new Nyuugan("Validator: min modifier used without a parameter.");
                        }
                        if (in_array('float', $allTokens) || in_array('int', $allTokens)) { // test as numeric
                            $val = (float) $userInput;
                            $minval = (float) $parameter;
                            if ($val < $minval) {
                                $errors[] = "The numeric field {$key} must be greater than or equal to {$minval}.";
                                continue;
                            }
                        } else { // test as string
                            if (strlen($userInput) < (int)$parameter) {
                                $errors[] = "The field {$key} must be longer than {$parameter} characters.";
                                continue;
                            }
                        }
                                         break;
                    case ('email'):
                        if (!filter_var($userInput, FILTER_VALIDATE_EMAIL)) {
                            $errors[] = "The field {$key} must be a valid e-mail.";
                            continue;
                        }
                                         break;
                    case ('alphanumeric_underscore_dash'):
                        if (!preg_match('/^[a-zA-Z0-9_-]*$/', $userInput)) {
                            $errors[] = "The field {$key} must consist solely of alphanumeric characters, underscores and dashes.";
                            continue;
                        }
                        break;
                    case ('url'):
                        if (!filter_var($userInput, FILTER_VALIDATE_URL)) {
                            $errors[] = "The field {$key} must be a valid URL.";
                            continue;
                        }
                        break;
                    case ('matches'):
                        if (strlen($parameter) == 0) {
                            throw new Nyuugan("Validator: matches modifier used without a parameter.");
                        }
                        if ($userInput != $input[$parameter][0]) {
                            $errors[] = "The field {$key} must match field {$parameter}.";
                            continue;
                        }
                        break;
                    default:
                        if (in_array($token, array_keys($this->plugins))) {
                            $errorMessage = $this->plugins[$token][0];
                            $callback = $this->plugins[$token][1];
                            $nParam = $this->plugins[$token][2];
                            $parameterArray = [];
                            if ($nParam == 0) {
                                $result = $callback($userInput);
                            } else {
                                $parameterArray = [$userInput];
                                foreach (explode(',', $parameter) as $param) {
                                    array_push($parameterArray, $param);
                                }
                                $result = call_user_func_array($callback, $parameterArray);
                            }
                            if (!($result === true)) {
                                $errorMessage = sprintf($errorMessage, $key);   // Replace %s with the field name
                                for ($i = 1; $i <= $nParam; $i++) {             // Replace \1, \2... with parameters
                                    $errorMessage = str_replace('\\' . $i, $parameterArray[$i], $errorMessage);
                                }
                                $errorMessage = str_replace('\\' . 0, $result, $errorMessage); // Replace \0 with the return value
                                $errors[] = $errorMessage;
                            }
                        } else {
                            throw new Nyuugan("Validator: Unknown modifier: {$token}.");
                        }
                }
            }
        }
        $this->errors = $errors;
    }

    public function passes()
    {
        return count($this->errors) == 0;
    }

    public function fails()
    {
        return !$this->passes();
    }

    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * @param string|array $name    The name of the filter. Either a string, e.g. `customfilter` or an array
     *                              if the filter accepts parameters in which case the number of parameters
     *                              is the second element in the array, e.g. [between, 2] used as between(4,5)
     * @param string $errorMessage  The error message with the placeholder %s representing the name of the field
     *                              and \1 \2 \3 ... optionally representing the parameters to the filter. The special
     *                              placeholder \0 is replaced by the return value for the validation function if it
     *                              does not return boolean true.
     * @param callable $callback    The function which does the validation. The function should return boolean true
     *                              for valid input. Anything different than === true is considered failed validation.
     *                              The first argument is the user input and any remaining arguments are the parameters
     *                              to the filter, e.g. for `between` with two parameters use
     *                              function ($userInput, $atLeast, $atMost) {...}
     *                              The return value for the function can be used to replace the \0 placeholder in the
     *                              error message, for example: `The string %s contains the banned word \0`.
     */
    public function registerPlugin($name, $errorMessage, callable $callback)
    {
        if (is_string($name)) {
            $this->plugins[$name] = [$errorMessage, $callback, 0];
        } else if (is_array($name)) {
            list($realName, $nParam) = $name;
            $this->plugins[$realName] = [$errorMessage, $callback, $nParam];
        }
    }
}
