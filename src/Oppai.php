<?php namespace Dekapai;

use Dekapai\Config\Config;
use function Dekapai\Hinnyuu\Facade\hinnyuu;
use Dekapai\Router\Router;
use Dekapai\Middleware\MiddlewareStack;
use Dekapai\Security\IEncryption;
use Dekapai\Security\OpenSSL;
use Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class Oppai
{

    /**
     * @var Router
     */
    private $router;

    /**
     * @var callable
     */
    private $routeCollection;

    /**
     * @var Request
     */
    public $request;

    /**
     * @var Response
     */
    public $response;

    /**
     * @var MiddlewareStack
     */
    private $middlewareStack;

    /**
     * @var ExceptionHandler[]
     */
    private $exceptionHandlers = [];

    /**
     * @var ExceptionHandler|null
     */
    private $uncaughtExceptionHandler = null;

    /**
     * @var Config
     */
    private $config;

    private $env = null;

    private $landingPageSubdir = '';

    private $siteSubdir = '';

    public function getUri(Request $request)
    {
        $uri = $this->getSanitizedUri('/' . trim($request->getBaseUrl() . $request->getPathInfo(), '/'));
        return $uri;
    }

    public function registerMiddleware($middleware, $priority = 0)
    {
        $this->middlewareStack->push($middleware, $priority);
    }

    public function registerExceptionHandler($class, $closure)
    {
        $this->exceptionHandlers[] = new ExceptionHandler($class, $closure);
    }

    public function setUncaughtExceptionHandler($closure)
    {
        $this->uncaughtExceptionHandler = new ExceptionHandler(Exception::class, $closure);
    }

    public function __construct(callable $routeCollection, Config $config)
    {
        $this->landingPageSubdir = trim($config->get('landingpage.subdir'), '/');
        $this->siteSubdir = trim($config->get('site.subdir'), '/');
        $this->routeCollection = $routeCollection;
        $this->config = $config;

        hinnyuu([IEncryption::class => OpenSSL::class]);
        hinnyuu([Request::class => function() { return $this->request; }]);
        hinnyuu([Config::class => function() { return $this->config; }]);

        // register some helper functions for use in controllers
        // fuck it.
        //hinnyuu(['getUri' => function(Request $request) { return $this->getUri($request); }]);
        hinnyuu(['getSiteroot' => function() { return $this->siteroot(); }]);
        hinnyuu(['getSiteSubdir' => function() { return $this->siteSubdir; }]);
        hinnyuu(['getLandingPageSubdir' => function() { return $this->landingPageSubdir; }]);
        hinnyuu(['getPath' => function() { return $this->path(); }]);

        $this->middlewareStack = new MiddlewareStack();
    }

    public function dispatch($request = null, $return = false)
    {
        if ($request == null) {
            $this->request = Request::createFromGlobals();
        } else {
            $this->request = $request;
        }
        // fuck it.
        $this->request->attributes->set('uri', $this->getUri($this->request));

        $this->response = new Response();

        // handle custom http verbs
        $method = $this->request->getMethod();

        if ($method == 'POST') {
            if ($this->request->request->has('_method')) {
                $method = $this->request->request->get('_method');
                if (!in_array($method, array('PUT', 'DELETE', 'PATCH'))) $method = 'POST';
            }
            if ($this->request->request->has('_xsrf')) {
                $this->request->headers->set('If-Match', $this->request->request->get('_xsrf'));
                $this->request->request->remove('_xsrf');
            }
        }
        $this->router = new Router($this->routeCollection, $this->getUri($this->request), $method);
        $this->momimomi();

        if ($return) {
            return $this->response;
        } else {
            if (!is_null($this->response)) $this->response->send();
            return null;
        }
    }


    private function momimomi()
    {
        try {

            $route = $this->router->resolveRoute();
            $controllerName = $route->getController();
            $methodName = $route->getMethod();

            $reflectionMethod = new \ReflectionMethod($controllerName, $methodName);
            $decorators = static::parseDocComment($reflectionMethod->getDocComment());

            foreach ($route->getRouteMiddleware() as $middleware) {
                if (is_string($middleware)) {
                    $this->middlewareStack->push($middleware, 0);
                } elseif (is_array($middleware)) {
                    $this->middlewareStack->push($middleware['class'], $middleware['priority']);
                }
            }
            $routeParameters = $route->getParameters();

            $controller = function() use ($controllerName, $routeParameters, $methodName) {
                return $this->invokeController($controllerName, $routeParameters, $methodName);
            };

            $result = $this->middlewareStack->execute($this->request, $this->response, $decorators + $routeParameters, $controller);
            $this->response = $result;

        } catch (Exception $e) {

            $foundHandler = false;

            foreach ($this->exceptionHandlers as $h) {
                if ($e instanceof $h->class) {
                    if (!$foundHandler) {
                        $this->response = $this->handleException($h, $e);
                        $foundHandler = true;
                        break;
                    }
                }
            }

            if (!is_null($this->uncaughtExceptionHandler)) {
                if (!$foundHandler) {
                    $this->response = $this->handleException($this->uncaughtExceptionHandler, $e);
                }
                $foundHandler = true;
            }

            if (!$foundHandler) {
                throw $e;
            } else {

            }
        }
    }

    // a fine example of how not to do srp
    public static function parseDocComment($comment)
    {
        $lines = preg_split('/\n/', $comment);
        $retVal = array();
        for ($i = 0; $i < count($lines); $i++) {
            $line = trim($lines[$i]);
            $line = rtrim($line, '*/');
            $line = ltrim($line, '* ');
            $line = preg_replace('/^\/\*\*/', '', $line);
            $line = preg_replace('/\*\/$/', '', $line);
            $lines[$i] = $line;
        }
        $comment = implode(" ", $lines);
        preg_match_all('/@([^ \{]+)\b([^@]*)/', $comment, $matches);
        $i = 0;
        while (isset($matches[1][$i])) {
            $key = trim($matches[1][$i]);
            $value = trim($matches[2][$i]);
            if (in_array(substr($value, 0, 1), ['{', '['])) {
                $value = AnnotationsProcessor::process($value);
            }
            $retVal[$key] = $value;
            $i++;
        }
        return $retVal;
    }

    private function invokeController($controllerName, array $parameters, $methodName)
    {
        return $this->responseWrapper( hinnyuu('#' . $controllerName, $methodName, $parameters) );
    }

    private function handleException(ExceptionHandler $h, Exception $e)
    {
        $closure = $h->closure;
        list($_cntrl, $_args, $_method) = $closure($e);
        /** @var Response $response */
        $response = $this->invokeController($_cntrl, $_args, $_method);
        return $response;
    }

    /**
     * @param mixed $response
     * @return Response
     */
    private function responseWrapper($response)
    {
        if (is_null($response)) {
            return new Response();
        } else if ($response instanceof Response) {
            return $response;
        } else {
            $jsonResponse = new JsonResponse();
            $jsonResponse->setData($response);
            return $jsonResponse;
        }
    }

    public function getSanitizedUri($uri)
    {
        $uri = substr($uri, strlen($this->path()) + 1);
        return filter_var(rtrim($uri, '/'), FILTER_SANITIZE_URL);
    }

    public function path()
    {
        if ($this->siteSubdir == '' && $this->landingPageSubdir == '') return '';
        if ($this->siteSubdir == '' && $this->landingPageSubdir != '') return '/' . $this->landingPageSubdir;
        if ($this->siteSubdir != '' && $this->landingPageSubdir == '') return '/' . $this->siteSubdir;
        return '/' . $this->siteSubdir . '/' . $this->landingPageSubdir;
    }

    public function siteroot()
    {
        $docRoot = $this->request->server->get('DOCUMENT_ROOT');
        return realpath(implode('/', array($docRoot, $this->siteSubdir))) . '/';
    }

}
