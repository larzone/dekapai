<?php

namespace {

    use function Dekapai\Hinnyuu\Facade\hinnyuu;
    use Dekapai\TemplateBuilder;
    use Symfony\Component\HttpFoundation\RedirectResponse;
    use Symfony\Component\HttpFoundation\Response;

    if (!function_exists('sign')) {

        function sign($x)
        {
            $x = (float)$x;
            if ($x < 0) return -1;
            if ($x > 0) return 1;
            return 0;
        }

    }

    if (!function_exists('esc')) {

        function esc($str)
        {
            return htmlspecialchars($str, ENT_QUOTES, 'UTF-8');
        }

    }

    if (!function_exists('redirect'))
    {
        function redirect($uri, $movedPermanently = false)
        {
            if ($movedPermanently) {
                $status = 301;
            } else {
                $status = 302;
            }
            $uri = '/' . ltrim($uri, '/');
            $response = new RedirectResponse(path() . $uri, $status);
            return $response;
        }
    }

    if (!function_exists('view'))
    {
        function view($templateName, $context = array(), $code = Response::HTTP_OK, $baseTemplate = 'index', $contentType = 'text/html')
        {
            return new TemplateBuilder($templateName, $context, $code, $baseTemplate, $contentType);
        }
    }

    if (!function_exists('siteroot'))
    {
        function siteroot()
        {
            return hinnyuu('getSiteroot');
        }
    }

    if (!function_exists('assetroot')) {

        // use for printing links to static assets from within the current landing page's "domain"
        function assetroot()
        {
            if (hinnyuu('getSiteSubdir') == '') return '';
            return '/' . hinnyuu('getSiteSubdir');
        }
    }

    if (!function_exists('asset'))
    {
        function asset($url)
        {
            return assetroot() . '/assets/' . $url;
        }
    }

    if (!function_exists('absurl'))
    {
        function absurl($url)
        {
            return assetroot() . '/' . $url;
        }
    }

    if (!function_exists('hash_equals')) {
        // forward compatibility with php 5.6
        function hash_equals($a, $b)
        {
            $la = mb_strlen($a, '8bit');
            $lb = mb_strlen($b, '8bit');
            $result = $la == $lb;
            for ($i = 0; $i < min($la, $lb); $i++) {
                $result = $result && mb_substr($a, $i, 1, '8bit') == mb_substr($b, $i, 1, '8bit');
            }
            return $result;
        }
    }

    if (!function_exists('path'))
    {
        function path()
        {
            return hinnyuu('getPath');
        }
    }

    if (!function_exists('url'))
    {
        function url($url)
        {
            return path() . '/' . $url;
        }
    }

}

namespace Dekapai\Session {

    use Dekapai\FlashMessage;
    use function Dekapai\Hinnyuu\Facade\hinnyuu;
    use Dekapai\Session;

    if (!function_exists('Dekapai\Session\flash')) {
        function flash($message, $severity)
        {
            /**
             * @var Session $session
             */
            $session = hinnyuu(Session::class);
            $flashMessage = new FlashMessage();
            $session->flash($flashMessage->withSeverity($severity)->withText($message));
        }
    }

    if (!function_exists('Dekapai\Session\get_flash')) {
        function get_flash($key, $default = null)
        {
            /**
             * @var Session $session
             */
            $session = hinnyuu(Session::class);
            return $session->getFlash($key, $default);
        }
    }

    if (!function_exists('Dekapai\Session\get_flash_messages')) {
        function get_flash_messages()
        {
            /**
             * @var Session $session
             */
            $session = hinnyuu(Session::class);
            return $session->getFlashMessages();
        }
    }

    if (!function_exists('Dekapai\Session\has_flash')) {
        function has_flash($key)
        {
            /**
             * @var Session $session
             */
            $session = hinnyuu(Session::class);
            return $session->hasFlash($key);
        }
    }

    if (!function_exists('Dekapai\Session\flash_value')) {
        function flash_value($key, $value)
        {
            /**
             * @var Session $session
             */
            $session = hinnyuu(Session::class);
            $session->flashValue($key, $value);
        }
    }
}
