<?php namespace Dekapai\CacheEngine;

use Dekapai\Config\Config;
use Dekapai\Exceptions\KeyNotFoundException;
use Dekapai\Interfaces\IKeyValueStore;

class MemcachedStore implements IKeyValueStore
{
    private $memcached;

    public function __construct(Config $config)
    {
        $this->memcached = new \Memcached();
        $this->memcached->addServer(
            $config->get('memcached.server'),
            $config->get('memcached.port')
        );
    }

    public function get($key)
    {
        $value = $this->memcached->get($key);
        if ($this->memcached->getResultCode() == 0) {
            return $value;
        }
        throw new KeyNotFoundException("Key not found: {$key}.");
    }

    public function invalidate($key)
    {
        $this->memcached->delete($key);
    }

    public function set($key, $value, $lifetime)
    {
        $this->memcached->set($key, $value, $lifetime);
    }
    
    public function wipe()
    {
        $this->memcached->flush();
    }
}
