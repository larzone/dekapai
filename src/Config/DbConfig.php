<?php namespace Dekapai\Config;

use Dekapai\Exceptions\Nyuugan;

class DbConfig extends AbstractConfig
{
    protected function getDbConfValue($key, $table, $keyCol, $valueCol)
    {
        throw new Nyuugan("DbConfig cannot access a database to retrieve config values.");
    }

    protected function addDbSource($table, $keyCol, $valueCol)
    {
        throw new Nyuugan("DbConfig cannot have database sources.");
    }
}
