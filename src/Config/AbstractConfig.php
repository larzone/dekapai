<?php namespace Dekapai\Config;

use Dekapai\Exceptions\Nyuugan;
use function Dekapai\Hinnyuu\Facade\hinnyuu;

abstract class AbstractConfig
{
    private $conf = null;
    protected $dbSources = array();
    private $jsonSources = array();
    private $envSources = array();
    private $isApcEnabled = false;
    private $isInitialized = false;

    public function __construct(array $sources)
    {
        if (isset($sources['db'])) {
            if (!is_array($sources['db']))
                throw new Nyuugan("Invalid config: db must be an array");
            foreach ($sources['db'] as $db) {
                if (!is_array($db) || count($db) != 3)
                    throw new Nyuugan("Invalid config: db must be an array of length 3");
                $this->addDbSource($db[0], $db[1], $db[2]);
            }
        }

        if (isset($sources['json'])) {
            if (!is_array($sources['json']))
                throw new Nyuugan("Invalid config: json must be an array");
            foreach ($sources['json'] as $json) {
                if (!is_string($json))
                    throw new Nyuugan("Invalid config: json must be a string");
                $this->addJsonSource($json);
            }
        }

        if (isset($sources['path'])) {
            if (!is_array($sources['path']))
                throw new Nyuugan("Invalid config: path must be an array");
            foreach ($sources['path'] as $path) {
                if (!is_string($path))
                    throw new Nyuugan("Invalid config: path must be a string");
                $this->addPathSource($path);
            }
        }

        if (isset($sources['array'])) {
            if (!is_array($sources['array']))
                throw new Nyuugan("Invalid config: array must be an array");
            $this->addArraySource($sources['array']);
        }

        if (isset($sources['env'])) {
            if (!is_array($sources['env']))
                throw new Nyuugan("Invalid config: env must be an array");
            foreach ($sources['env'] as $key => $val) {
                $this->addEnvSource($key, $val);
            }
        }
    }

    public function init()
    {
        $this->isApcEnabled = function_exists('apcu_fetch');
        $this->isInitialized = true;
    }

    // array of key-value pairs
    private function addArraySource(array $source)
    {
        foreach ($source as $key => $value) {
            $this->conf[$key] = $value;
        }
    }

    private function addEnvSource($confKey, $envVar)
    {
        $this->envSources[$confKey] = $envVar;
    }

    // path to php file that returns an array of key-value pairs
    private function addPathSource($source)
    {
        if (!is_readable($source)) throw new Nyuugan("Filename does not exist: {$source}");
        $this->addArraySource(require $source);
    }

    // path to a json file
    private function addJsonSource($source)
    {
        if (!is_readable($source)) throw new Nyuugan("Filename does not exist: {$source}");
        // lazy loading
        $this->jsonSources[] = $source;
    }

    // database table with key and value columns (lazy loading) - these have lower priority
    protected abstract function addDbSource($table, $keyCol, $valueCol);

    // lazy loading
    public function get($key)
    {
        if (isset($this->conf[$key])) return $this->conf[$key];

        // --- path sources are already loaded

        // --- check env

        if (isset($this->envSources[$key])) {
            $value = getenv($this->envSources[$key]);
            $this->conf[$key] = $value;
            return $value;
        }

        // --- load json sources

        foreach ($this->jsonSources as $json) { // load all json sources into $this->conf
            $this->addArraySource(json_decode(file_get_contents($json), true));
        }
        $this->jsonSources = []; // prevent reloading the same file(s)
        if (isset($this->conf[$key])) return $this->conf[$key];

        // --- load db sources
        foreach ($this->dbSources as $dbSource) {
            $dbResult = $this->getDbConfValue(
                $key,
                $dbSource['table'],
                $dbSource['keyCol'],
                $dbSource['valueCol']
            );
            if (!is_null($dbResult)) {
                $this->conf['key'] = $dbResult;
                return $dbResult;
            }
        }
        throw new Nyuugan("Config variable not found: {$key}.");
    }

    /**
     * @param string $key
     * @return int
     */
    public function getInt($key)
    {
        return (int) $this->get($key);
    }

    /**
     * @param string $key
     * @return float
     */
    public function getFloat($key)
    {
        return (float) $this->getFloat($key);
    }

    protected abstract function getDbConfValue($key, $table, $keyCol, $valueCol);
}
