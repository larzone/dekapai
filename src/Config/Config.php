<?php namespace Dekapai\Config;

use Dekapai\Database\MysqlDb;
use Dekapai\Exceptions\Nyuugan;
use function Dekapai\Hinnyuu\Facade\hinnyuu;

class Config extends AbstractConfig
{

    protected function getDbConfValue($key, $table, $keyCol, $valueCol)
    {
        /**
         * @var MysqlDb $db
         */
        $db = hinnyuu(MysqlDb::class);
        $sql = "SELECT {$valueCol} FROM {$table} WHERE {$keyCol} = :key";
        $sth = $db->prepare($sql);
        $sth->execute(array('key' => $key));
        $row = $sth->fetch();
        if (empty($row) || !isset($row['value'])) {
            return null;
        }
        $sth->closeCursor();
        return $row['value'];
    }

    // database table with key and value columns (lazy loading) - these have lower priority
    protected function addDbSource($table, $keyCol, $valueCol)
    {
        // basic input sanitation
        $pattern = '/^[a-zA-Z_][a-zA-Z0-9_]*$/';
        if (!preg_match($pattern, $table)) throw new Nyuugan("Invalid table name");
        if (!preg_match($pattern, $keyCol)) throw new Nyuugan("Invalid key column name");
        if (!preg_match($pattern, $valueCol)) throw new Nyuugan("Invalid value column name");
        $this->dbSources[] = array(
            'table' => $table,
            'keyCol' => $keyCol,
            'valueCol' => $valueCol
        );
    }

}
