<?php namespace Dekapai;

use Dekapai\Exceptions\Nyuugan;

class AnnotationsProcessor
{
    public static function process($annotation)
    {
        $annotation = trim($annotation);
        $sLen = strlen($annotation);
        if ($sLen == 0) {
            return null;
        }
        $result = json_decode($annotation, true);
        if (json_last_error() !== 0) {
            throw new Nyuugan("Parse Annotation Failed: " . $annotation);
        }
        return $result;
    }
}
