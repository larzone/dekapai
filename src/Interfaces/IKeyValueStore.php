<?php namespace Dekapai\Interfaces;


interface IKeyValueStore
{
    public function get($key);
    public function set($key, $value, $lifetime);
    public function invalidate($key);
    public function wipe();
    
}
