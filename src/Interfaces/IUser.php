<?php namespace Dekapai\Interfaces;

interface IUser
{
    public function auth($remoteAddr);
    public function id();
    public function login($uid, $hash, $remoteAddr);
    public function perm($perm);
}
