<?php namespace Dekapai\Interfaces;

interface IRowCountable
{
    public function getRowCount($table, $column, $testEqual);
}
