<?php namespace Dekapai;

class FlashMessage
{

    private $severity = null;
    private $icon = null;
    private $text = null;

    public function __construct()
    {

    }

    public function withSeverity($severity)
    {
        if (!in_array($severity, array('warning', 'information', 'error', 'notification', 'success'))) {
            throw new \InvalidArgumentException("Invalid severity type: {$severity}.");
        }
        $this->severity = $severity;
        return $this;
    }

    public function withIcon($icon)
    {
        $this->icon = $icon;
        return $this;
    }

    public function withText($text)
    {
        $this->text = $text;
        return $this;
    }

    public function getValue()
    {
        $severity = !empty($this->severity) ? $this->severity : 'information';
        $icon = !empty($this->icon) ? $this->icon : 'fa-check';
        $text = !empty($this->text) ? $this->text : 'NOTIFICATION';
        $textClass = '';
        switch ($severity) {
            case('warning'):
                $textClass = 'text-warning';
                break;
            case('information'):
                break;
            case('error'):
                $textClass = 'text-danger';
                break;
            case('notification'):
                $textClass = '';
                break;
            case('success'):
                $textClass = 'text-success';
                break;
        }
        return array('severity' => $severity, 'icon' => $icon, 'text' => $text, 'textClass' => $textClass);
    }

}
