<?php namespace Dekapai;

class ExceptionHandler
{

    public $class;
    public $closure;

    public function __construct($className, $closure)
    {
        $this->class = $className;
        $this->closure = $closure;
    }

}
