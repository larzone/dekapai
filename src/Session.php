<?php namespace Dekapai;

use Dekapai\Config\Config;
use Dekapai\Security\IEncryption;
use InvalidArgumentException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\Session\SessionBagInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpFoundation\Session\Storage\MetadataBag;

class Session implements SessionInterface
{
    private $data = [];
    private $started = false;
    private $rawcookie = '';
    private $changed = false;

    private $id;
    private $isInvalid = false;
    private $expireCookie = null;

    private $defaultExpireDays = 30;
    private $customExpire = null;      // Passed directly to Cookie constructor if set

    private $namespace = 'def';

    private $cookieName = 'SESS';
    private $path = '/';

    const FLASH_PREFIX = 'XF_'; // to not collide flash and normal session variables
    const FLASH_MESSAGE_VAR = 'MSG';
    const EXPIRE = 600; // expire flash values - 10 min
    const SID_LEN = 8;

    private $config;
    private $crypto;
    private $request;

    public function __construct(Config $config, IEncryption $crypto, Request $request)
    {
        $this->config = $config;
        $this->crypto = $crypto;
        $this->request = $request;
    }

    public function isStarted()
    {
        return $this->started;
    }

    public function setNamespace(string $namespace)
    {
        if (strlen($namespace) < 1 || strlen($namespace) > 20)
            throw new \InvalidArgumentException("Namespace too long or too short");
        $this->namespace = $namespace;
    }

    public function getNamespace()
    {
        return $this->namespace;
    }

    private function setInCurrentNamespace($key, $value)
    {
        if (!isset($this->data[$this->namespace])) $this->data[$this->namespace] = [];
        $this->data[$this->namespace][$key] = $value;
        $this->changed = true;
    }

    private function hasInCurrentNamespace($key)
    {
        if (!isset($this->data[$this->namespace])) return false;
        if (!isset($this->data[$this->namespace][$key])) return false;
        return true;
    }

    private function getInCurrentNamespace($key)
    {
        if (!isset($this->data[$this->namespace])) return null;
        if (!isset($this->data[$this->namespace][$key])) return null;
        return $this->data[$this->namespace][$key];
    }

    private function unsetInCurrentNamespace($key)
    {
        if (!$this->hasInCurrentNamespace($key)) return;
        unset($this->data[$this->namespace][$key]);
        $this->changed = true;
    }

    public function flash(FlashMessage $flashMessage)
    {
        $key = Session::FLASH_PREFIX . Session::FLASH_MESSAGE_VAR;
        $value = $flashMessage->getValue();
        if ($this->hasValue($key)) {
            $newvalue = $this->getValue($key);
            $newvalue['exp'] = time() + Session::EXPIRE;
            $newvalue['val'][] = $value;
            $this->setValue($key, $newvalue);
        } else {
            $newvalue = array();
            $newvalue['exp'] = time() + Session::EXPIRE;
            $newvalue['val'] = array();
            $newvalue['val'][] = $value;
            $this->setValue($key, $newvalue);
        }
        $this->changed = true;
    }

    public function getFlashMessages()
    {
        $val = $this->getFlash(Session::FLASH_MESSAGE_VAR);
        if (empty($val)) $val = array();
        return $val;
    }

    public function flashValue($key, $value)
    {
        if ($key == Session::FLASH_MESSAGE_VAR) throw new \Exception(sprintf("%s is protected", Session::FLASH_MESSAGE_VAR));
        $key = self::FLASH_PREFIX . $key;
        $this->setValue($key, array('exp' => time() + Session::EXPIRE, 'val' => $value));
        $this->changed = true;
    }

    public function getFlash($key, $defaultValue = null)
    {
        $key = self::FLASH_PREFIX . $key;
        if ($this->hasValue($key)) {
            $retVal = $this->getValue($key);
            $this->unsetInCurrentNamespace($key);
            $this->changed = true;
            return $retVal['val'];
        }
        return $defaultValue;
    }

    public function hasFlash($key)
    {
        $key = self::FLASH_PREFIX . $key;
        if ($this->hasInCurrentNamespace($key)) {
            if ($this->getInCurrentNamespace($key)['exp'] > time()) {
                return true;
            }
        }
        return false;
    }

    public function setValue($key, $value)
    {
        $this->setInCurrentNamespace($key, $value);
        $this->changed = true;
    }

    public function getValue($key, $default = null)
    {
        if ($this->hasInCurrentNamespace($key)) {
            return $this->getInCurrentNamespace($key);
        }
        return $default;
    }

    public function hasValue($key)
    {
        return $this->hasInCurrentNamespace($key);
    }

    public function start()
    {
        if ($this->started) return;
        if ($this->request->cookies->has($this->cookieName)) {
            $this->rawcookie = $this->request->cookies->get($this->cookieName);
            $serialized = $this->crypto->decrypt(
                $this->rawcookie,
                $this->config->get('app.security.key'),
                $this->config->get('app.security.salt'));
            if ($serialized === false) {
                $this->data = array();
                $this->changed = true;
            }
            else {
                $data = json_decode($serialized, true);
                if (json_last_error() !== JSON_ERROR_NONE) $data = [];
                if (isset($data['i']) && strlen($data['i'])) {
                    $this->data = $data['d'];
                    $this->id = $data['i'];
                } else {
                    $this->data = [];
                }
            }
        } else {
            $this->data = array();
            $this->changed = true;
        }
        if (is_null($this->id) || strlen($this->id) != static::SID_LEN) {
            $this->id = static::generateId(static::SID_LEN);
            $this->changed = true;
        }
        if (!isset($this->data[$this->namespace])) $this->data[$this->namespace] = [];
        $this->started = true;
    }

    private function cleanExpiredFlash()
    {
        if (!isset($this->data[$this->namespace])) return;
        foreach ($this->data[$this->namespace] as $key => $value) {
            if (preg_match('/^' . Session::FLASH_PREFIX . '/', $key)) {
                $exp = $value['exp'];
                if ($exp < time()) {
                    unset($this->data[$this->namespace][$key]);
                }
            }
        }
    }

    public function finalize(Response $response)
    {
        if ($this->started) {
            $this->cleanExpiredFlash();
            if (is_null($this->id) || strlen($this->id) != static::SID_LEN) $this->data = [];
            $serialized = json_encode(['i' => $this->id, 'd' => $this->data]);
            $this->rawcookie = $this->crypto->encrypt(
                $serialized,
                $this->config->get('app.security.key'),
                $this->config->get('app.security.salt'));
            if (!$this->isInvalid) {
                if (!is_null($this->customExpire)) {
                    $expire = $this->customExpire;
                } else {
                    $expire = new \DateTime();
                    $diff = new \DateInterval(sprintf("P%dD", $this->defaultExpireDays));
                    $expire->add($diff);
                }
            } else {
                $expire = new \DateTime();
                $diff = new \DateInterval(sprintf("PT%dS", abs(intval($this->expireCookie))));
                if ($this->expireCookie >= 0) { $expire->add($diff); }
                else { $expire->sub($diff); }
            }
            $response->headers->setCookie(
                new Cookie(
                    $this->cookieName,
                    $this->rawcookie,
                    $expire,
                    $this->path
                )
            );
        }
    }

    public function destroy()
    {
        $this->data[$this->namespace] = array();
        $this->changed = true;
    }

    public static function generateId($token_length = 32)
    {
        if ($token_length <= 0) throw new InvalidArgumentException("Token length must be positive.");
        if ($token_length > 1e7) throw new InvalidArgumentException("Token length too large.");

        $chars =  "abcdefghijklmnopqrstuvwxyz";
        $chars .= "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $chars .= "0123456789";
        $n_chars = strlen($chars);
        $token = "";
        $buf = "";

        if (function_exists('random_bytes')) {
            $buf = random_bytes($token_length);
        } else if (is_readable('/dev/urandom')) {
            $f = fopen('/dev/urandom', 'r');
            $buf = fread($f, $token_length);
            fclose($f);
        }

        if (strlen($buf) > 0) {
            for ($i = 0; $i < $token_length; $i++) {
                $token .= $chars[ord($buf{$i}) % $n_chars];
            }
        } else {
            mt_srand();
            for ($i = 0; $i < $token_length; $i++) {
                $idx = mt_rand(0, $n_chars - 1);
                $token .= substr($chars, $idx, 1);
            }
        }
        return $token;
    }

    /**
     * Returns the session ID.
     *
     * @return string The session ID.
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Sets the session ID.
     *
     * @param string $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * Returns the session name.
     *
     * @return mixed The session name.
     */
    public function getName()
    {
        return $this->cookieName;
    }

    /**
     * Sets the session name.
     *
     * @param string $name
     * @throws \Exception
     */
    public function setName($name)
    {
        $this->cookieName = $name;
    }

    public function setPath($path)
    {
        $this->path = $path;
    }

    public function getPath()
    {
        return $this->path;
    }

    public function setExpire($expire)
    {
        $this->customExpire = $expire;
    }

    /**
     * Invalidates the current session.
     *
     * Clears all session attributes and flashes and regenerates the
     * session and deletes the old session from persistence.
     *
     * @param int $lifetime Sets the cookie lifetime for the session cookie. A null value
     *                      will leave the system settings unchanged, 0 sets the cookie
     *                      to expire with browser session. Time is in seconds, and is
     *                      not a Unix timestamp.
     *
     * @return bool True if session invalidated, false if error.
     */
    public function invalidate($lifetime = null)
    {
        $this->isInvalid = true;
        $this->destroy();
    }

    /**
     * Migrates the current session to a new session id while maintaining all
     * session attributes.
     *
     * @param bool $destroy Whether to delete the old session or leave it to garbage collection.
     * @param int $lifetime Sets the cookie lifetime for the session cookie. A null value
     *                       will leave the system settings unchanged, 0 sets the cookie
     *                       to expire with browser session. Time is in seconds, and is
     *                       not a Unix timestamp.
     *
     * @return bool True if session migrated, false if error.
     */
    public function migrate($destroy = false, $lifetime = null)
    {
        return false;
    }

    /**
     * Force the session to be saved and closed.
     *
     * This method is generally not required for real sessions as
     * the session will be automatically saved at the end of
     * code execution.
     */
    public function save()
    {
        return false;
    }

    /**
     * Checks if an attribute is defined.
     *
     * @param string $name The attribute name
     *
     * @return bool true if the attribute is defined, false otherwise
     */
    public function has($name)
    {
        return $this->hasValue($name);
    }

    /**
     * Returns an attribute.
     *
     * @param string $name The attribute name
     * @param mixed $default The default value if not found.
     *
     * @return mixed
     */
    public function get($name, $default = null)
    {
        return $this->getValue($name, $default);
    }

    /**
     * Sets an attribute.
     *
     * @param string $name
     * @param mixed $value
     * @throws \Exception
     */
    public function set($name, $value)
    {
        $this->setValue($name, $value);
        $this->changed = true;
    }

    /**
     * Returns attributes.
     *
     * @return array Attributes
     */
    public function all()
    {
        return $this->data[$this->namespace];
    }

    /**
     * Sets attributes.
     *
     * @param array $attributes Attributes
     */
    public function replace(array $attributes)
    {
        foreach ($attributes as $key => $value) {
            $this->set($key, $value);
        }
        $this->changed = true;
    }

    /**
     * Removes an attribute.
     *
     * @param string $name
     *
     * @return mixed The removed value or null when it does not exist
     * @throws \Exception
     */
    public function remove($name)
    {
        $this->unsetInCurrentNamespace($name);
        $this->changed = true;
    }

    /**
     * Clears all attributes.
     */
    public function clear()
    {
        $this->data[$this->namespace] = [];
        $this->changed = true;
    }

    /**
     * Registers a SessionBagInterface with the session.
     *
     * @param SessionBagInterface $bag
     * @throws \Exception
     */
    public function registerBag(SessionBagInterface $bag)
    {
        throw new \Exception("Not implemented.");
    }

    /**
     * Gets a bag instance by name.
     *
     * @param string $name
     *
     * @return SessionBagInterface
     * @throws \Exception
     */
    public function getBag($name)
    {
        throw new \Exception("Not implemented.");
    }

    /**
     * Gets session meta.
     *
     * @return MetadataBag
     * @throws \Exception
     */
    public function getMetadataBag()
    {
        throw new \Exception("Not implemented.");
    }
}
