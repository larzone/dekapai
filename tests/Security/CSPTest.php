<?php namespace Security;

use Dekapai\Security\CSP;

class CSPTest extends \PHPUnit_Framework_TestCase
{
    /** @test */
    public function it_creates_a_string_or_else_it_gets_the_hose_again()
    {
        $this->assertEquals("default-src 'self' https://www.google.com; " .
                            "script-src 'self' 'unsafe-inline' https://cdnjs.cloudflare.com; " .
                            "img-src 'self' http://imgur.com 'none'; " .
                            "upgrade-insecure-requests",
            (string) (new CSP)->defaultSrc([ CSP::SELF, 'https://www.google.com' ])
                              ->scriptSrc ([ CSP::SELF, CSP::UNSAFE_INLINE, 'https://cdnjs.cloudflare.com'])
                              ->imgSrc    ([ CSP::SELF, 'http://imgur.com', CSP::NONE ])
                              ->upgradeInsecureRequests());
    }

    /** @test */
    public function it_creates_a_policy_from_an_array_or_else_it_gets_the_hose_again()
    {
        $this->assertEquals("default-src 'self' https://www.google.com; " .
            "script-src 'self' 'unsafe-inline' https://cdnjs.cloudflare.com; " .
            "img-src 'self' http://imgur.com 'none'; " .
            "upgrade-insecure-requests",
            (string) (CSP::fromArray( [
                'default-src' => [ "'self'", 'https://www.google.com' ],
                'script-src'  => [ CSP::SELF, "'unsafe-inline'", 'https://cdnjs.cloudflare.com'],
                'img-src'     => [ "'self'", 'http://imgur.com', CSP::NONE ],
                'upgrade-insecure-requests' => 34343 ]))); // can pass anything as the value
    }

}
