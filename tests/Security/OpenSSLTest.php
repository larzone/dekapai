<?php namespace Dekapai\Tests\Security;

use Dekapai\Security\OpenSSL;
use Dekapai\Session;
use PHPUnit_Framework_TestCase;

class OpenSSLTest extends PHPUnit_Framework_TestCase
{
    /** @test */
    public function it_encrypts_and_decrypts_or_else_it_gets_the_hose_again()
    {
        $secretMessage = Session::generateId(128);
        $key = Session::generateId(64);
        $salt = Session::generateId(64);
        $openSSL = new OpenSSL();
        $enc = $openSSL->encrypt($secretMessage, $key, $salt);
        $this->assertEquals($secretMessage, $openSSL->decrypt($enc, $key, $salt));
        $this->assertFalse($openSSL->decrypt($enc, $key, $salt."\0"));
    }
}
