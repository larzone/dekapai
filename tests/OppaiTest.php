<?php namespace Dekapai\Tests;

use Closure;
use Dekapai\Config\Config;
use Dekapai\Oppai;
use Dekapai\Controller;
use Dekapai\Middleware\Middleware;
use Dekapai\Router\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class Timer implements Middleware
{
    public function __invoke(Request $request, Response $response, Closure $next)
    {
        if (!defined('_TIME_START_')) define('_TIME_START_', 20);
        $response = $next($request, $response);
        $time = 30 - _TIME_START_;
        $response->headers->add(['X-Generated-In' => $time]);
        return $response;
    }
}

class MockGateway
{
    public $somePublicVar;
    public function __construct()
    {
        $this->somePublicVar = "x";
    }
}

class MockController extends Controller
{

    private $dogs = [];

    public function doStuff()
    {
        return 100;
    }

    public function getDogs(MockGateway $mockGateway, $dogs_per_page = 108, $pageno = 1, $user_id = -1, $freetext = '', $filter = '', $options = '')
    {
        if ($mockGateway->somePublicVar != 'x') {
            throw new \Exception("Mock gateway not initialized.");
        }
        if ($filter != 'type=any') {
            throw new \Exception("Invalid filter.");
        }
        if ($user_id != -1) {
            throw new \Exception("Invalid user id.");
        }
        if ($pageno != 724) {
            throw new \Exception("Invalid pageno.");
        }
        if ($freetext != '') {
            throw new \Exception("Invalid freetext.");
        }
        if ($options != 'dead=no,sortby=date') {
            throw new \Exception("Invalid options.");
        }
        if ($dogs_per_page != 24) {
            throw new \Exception("Invalid dogs per page.");
        }
        return "success";
    }

    public function newDog(Request $request)
    {
        $name = $request->request->get('name');
        $this->dogs[] = $name;
        return new Response("Created {$name}.", 201);
    }

    public function deleteDog(Request $request)
    {
        $name = $request->request->get('name');
        if (in_array($name, $this->dogs)) {
            $this->dogs = array_filter($this->dogs, function($dog) use ($name) {
                if ($dog == $name) return false;
                return true;
            });
            return new Response("Deleted {$name}.", 200);
        }
        return new Response("Not found: {$name}.", 404);
    }

}

class OppaiTest extends \PHPUnit_Framework_TestCase
{

    /**
     * @var Config
     */
    private $config;

    public function setUp()
    {
        $this->config = new Config([
            'array' => [
                'site.subdir' => 'mysite',
                'landingpage.subdir' => '',
                'env' => 'dev',
                'app.security.key' => 'something',
                'profile.dir' => ''
            ]
        ]);
    }

    /** @test */
    public function simple_test()
    {
        $routeCollection = function() {
            yield Route::GET(
                'something',
                [MockController::class, 'doStuff']
            );
        };

        $request = Request::create("mysite/something");

        $app = new Oppai( $routeCollection, $this->config, 'test' );
        $response = $app->dispatch($request, true);
        $this->assertEquals(json_encode(100), $response->getContent());
    }

    /** @test */
    public function it_calls_a_controller_with_correct_parameters_or_else_it_gets_the_hose_again()
    {
        $routeCollection = function() {
            yield Route::GET(
                '/api/dog/getdogs(/pageno::{pageno|int}|/user_id::{user_id|int}|/dogs_per_page::{dogs_per_page|int}|/filter::{filter}|/options::{options}|/freetext::{freetext})*',
                [MockController::class, 'getDogs']
            );
        };

        $request = Request::create( // why the fuck doesn't a single colon work?
            "/mysite/api/dog/getdogs/pageno::724/dogs_per_page::24/filter::type=any/options::dead=no,sortby=date"
        );

        $app = new Oppai( $routeCollection, $this->config, 'test' );
        $response = $app->dispatch($request, true);
        $this->assertEquals(json_encode("success"), $response->getContent());
    }

    /** @test */
    public function it_can_do_put_and_delete_requests_or_else_it_gets_the_hose_again()
    {
        $routeCollection = function() {
            yield Route::PUT(
                'api/dogs',
                [MockController::class, 'newDog']
            );
            yield Route::DELETE(
                'api/dogs',
                [MockController::class, 'deleteDog']
            );
        };

        $request = Request::create(
            "/mysite/api/dogs",
            'POST',
            ['_method' => 'PUT', 'name' => 'Snoopy']   // passing in _method
        );

        $app = new Oppai( $routeCollection, $this->config, 'test' );
        $response = $app->dispatch( $request, true );
        $this->assertEquals(201, $response->getStatusCode());
        $this->assertEquals("Created Snoopy.", $response->getContent());

        // can't maintain state between requests since the controller is instantiated for each request...

        $request = Request::create(
            "/mysite/api/dogs",
            'DELETE',                                 // using http verb instead of passing in _method
            ['name' => 'Snoopy']
        );

        $response = $app->dispatch( $request, true );

        $this->assertEquals(404, $response->getStatusCode());

    }

    /** @test */
    public function it_can_register_and_execute_middleware_before_and_after_the_controller_or_else_it_gets_the_hose_again()
    {
        $routeCollection = function() {
            yield Route::PUT(
                'api/dogs',
                [MockController::class, 'newDog']
            );
        };

        $request = Request::create(
            "/mysite/api/dogs",
            'POST',
            ['_method' => 'PUT', 'name' => 'Snoopy']   // passing in _method
        );

        $app = new Oppai( $routeCollection, $this->config, 'test' );

        $app->registerMiddleware(Timer::class);

        $response = $app->dispatch($request, true);
        $this->assertEquals(10, $response->headers->get('X-Generated-In'));

    }

}
