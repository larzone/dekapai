<?php namespace Dekapai\Tests\Database;

use Dekapai\Config\DbConfig;
use Dekapai\Database\MysqlDb;
use function Dekapai\Hinnyuu\Facade\hinnyuu;
use PDO;

class MysqlDbTest extends \PHPUnit_Framework_TestCase
{

    public function setUp()
    {
        hinnyuu([DbConfig::class => function() {
            return new DbConfig(['array' => [
                'db.hostname' => '',
                'db.database' => '',
                'db.username' => '',
                'db.password' => '',
            ]]);
        } ]);
    }

    /** @test */
    public function it_creates_the_instance_or_else_it_gets_the_hose_again()
    {
        $this->assertInstanceOf(PDO::class, hinnyuu(MysqlDb::class));
    }

}
