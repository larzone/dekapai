<?php

class IdentityMiddleware implements \Dekapai\Middleware\Middleware
{
    public function __invoke(\Symfony\Component\HttpFoundation\Request $request, \Symfony\Component\HttpFoundation\Response $response, Closure $next)
    {
        return $next($request, $response);
    }
}

class ResponseModifierMiddleware implements \Dekapai\Middleware\Middleware
{

    public function __invoke(\Symfony\Component\HttpFoundation\Request $request, \Symfony\Component\HttpFoundation\Response $response, Closure $next)
    {
        try {
            $response = $next($request, $response);
        }
        finally {
            $response->headers->set('X-Status', 123);
        }
        return $response;
    }
}

class MiddlewareStackTest extends \PHPUnit_Framework_TestCase
{
    /** @test */
    public function it_returns_the_response_or_else_it_gets_the_hose_again()
    {
        $stack = new \Dekapai\Middleware\MiddlewareStack();
        $request = new \Symfony\Component\HttpFoundation\Request();
        $response = new \Symfony\Component\HttpFoundation\Response();
        $controller = function() use ($response) { return $response; };
        $returnedResponse = $stack->execute($request, $response, [], $controller);
        $this->assertSame($returnedResponse, $response);
    }

    /** @test */
    public function it_propagates_the_exception_or_else_it_gets_the_hose_again()
    {
        $stack = new \Dekapai\Middleware\MiddlewareStack();
        $request = new \Symfony\Component\HttpFoundation\Request();
        $response = new \Symfony\Component\HttpFoundation\Response();
        $controller = function() { throw new InvalidArgumentException(); };
        try {
            $stack->execute($request, $response, [], $controller);
            $this->assertFalse(true);
        } catch (InvalidArgumentException $e) {
            $this->assertTrue(true);
        }
    }

    /** @test */
    public function it_runs_middleware_or_else_it_gets_the_hose_again()
    {
        $stack = new \Dekapai\Middleware\MiddlewareStack();
        $request = new \Symfony\Component\HttpFoundation\Request();
        $response = new \Symfony\Component\HttpFoundation\Response();
        $stack->push(IdentityMiddleware::class);
        $controller = function() use ($response) { return $response; };
        $returnedResponse = $stack->execute($request, $response, [], $controller);
        $this->assertSame($returnedResponse, $response);
    }

    /** @test */
    public function it_runs_two_middlewares_or_else_it_gets_the_hose_again()
    {
        $stack = new \Dekapai\Middleware\MiddlewareStack();
        $request = new \Symfony\Component\HttpFoundation\Request();
        $response = new \Symfony\Component\HttpFoundation\Response();
        $stack->push(IdentityMiddleware::class);
        $stack->push(ResponseModifierMiddleware::class);
        $controller = function() use ($response) { return $response; };
        $returnedResponse = $stack->execute($request, $response, [], $controller);
        $this->assertSame($returnedResponse, $response);
        $this->assertTrue($response->headers->has('X-Status'));
    }

    /** @test */
    public function it_runs_two_middlewares_and_propagates_an_exception_or_else_it_gets_the_hose_again()
    {
        $stack = new \Dekapai\Middleware\MiddlewareStack();
        $request = new \Symfony\Component\HttpFoundation\Request();
        $response = new \Symfony\Component\HttpFoundation\Response();
        $stack->push(IdentityMiddleware::class);
        $stack->push(ResponseModifierMiddleware::class);
        $controller = function() { throw new Exception(); };
        try {
            $stack->execute($request, $response, [], $controller);
            $this->assertFalse(true);
        }
        catch (Exception $e) {
            $this->assertTrue($response->headers->has('X-Status'));
        }
    }
}
