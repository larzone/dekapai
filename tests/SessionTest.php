<?php

namespace Wasuremono {

    use function Dekapai\Session\flash;
    use function Dekapai\Session\flash_value;
    use Symfony\Component\HttpFoundation\Request;

    class Dog
    {
        public function bark()
        {
            if (\Dekapai\Session\has_flash('someKey')) {}
            return "woof";
        }

        public function wag(Request $r)
        {
            $r->getSession()->set('foo', 'bar');
            return "woof";
        }

        public function logout(Request $r)
        {
            $r->getSession()->invalidate();
            return "bye";
        }

        public function flash()
        {
            flash('Hello', 'warning');
            flash('Baibai', 'error');
            return "flash";
        }
    }
}

namespace DekapaiTests {

    use Dekapai\Config\Config;
    use function Dekapai\Hinnyuu\Facade\hinnyuu;
    use Dekapai\Middleware\SessionMiddleware;
    use Dekapai\Oppai;
    use Dekapai\Router\Route;
    use Dekapai\Security\OpenSSL;
    use Dekapai\Session;
    use PHPUnit_Framework_TestCase;
    use Symfony\Component\HttpFoundation\Cookie;
    use Symfony\Component\HttpFoundation\Request;
    use Wasuremono\Dog;

    class SessionTest extends PHPUnit_Framework_TestCase
    {

        private $routeCollection;

        /** @var Oppai */
        private $app;

        /** @var OpenSSL */
        private $crypto;

        /** @var Config */
        private $config;

        public function setUp()
        {
            hinnyuu(\Dekapai\Hinnyuu\Hinnyuu::RESET);

            $this->config = new Config(['array' => [
                "app.session.namespace" => 'def',
                'site.subdir' => '',
                'landingpage.subdir' => '',
                'env' => 'test',
                'app.security.key' => Session::generateId(64),
                'app.security.salt' => Session::generateId(64),
                'profile.dir' => ''
            ]]);

            $this->routeCollection = function() {
                yield Route::GET(
                    'dogs/bark',
                    [Dog::class, 'bark'],
                    [SessionMiddleware::class]
                );
                yield Route::GET(
                    'dogs/wag',
                    [Dog::class, 'wag'],
                    [SessionMiddleware::class]
                );
                yield Route::GET(
                    'dogs/logout',
                    [Dog::class, 'logout'],
                    [SessionMiddleware::class]
                );
                yield Route::GET(
                    'dogs/flash',
                    [Dog::class, 'flash'],
                    [SessionMiddleware::class]
                );
            };

            $this->app = new Oppai($this->routeCollection, $this->config, 'test');

            $this->crypto = new OpenSSL();
        }

        /** @test */
        public function cookie_remains_same_if_nothing_changed()
        {
            $serialized = json_encode(['i' => str_repeat('a', 8), 'd' => ['def' => []]]);
            $rawCookie = $this->crypto->encrypt(
                $serialized,
                $this->config->get('app.security.key'),
                $this->config->get('app.security.salt'));

            $request = Request::create('dogs/bark', 'GET', [], ['SESS' => $rawCookie]);
            $response = $this->app->dispatch($request, true);
            $cookies = $response->headers->getCookies();
            $this->assertEquals(1, count($cookies));
            /** @var Cookie $cookie */
            $cookie = $cookies[0];
            $this->assertEquals('SESS', $cookie->getName());
            $this->assertEquals($serialized, $this->crypto->decrypt($cookie->getValue(),
                $this->config->get('app.security.key'),
                $this->config->get('app.security.salt')));
        }

        /** @test */
        public function cookie_is_set_if_something_changed()
        {
            $serialized = json_encode(['i' => str_repeat('a', 8), 'd' => ['myNamespace' => ['myKey' => 'myValue']]]);
            $rawCookie = $this->crypto->encrypt(
                $serialized,
                $this->config->get('app.security.key'),
                $this->config->get('app.security.salt'));

            $request = Request::create('dogs/wag', 'GET', [], ['SESS' => $rawCookie]);
            $response = $this->app->dispatch($request, true);

            $cookies = $response->headers->getCookies();
            $this->assertEquals(1, count($cookies));

            /** @var Cookie $cookie */
            $cookie = $cookies[0];

            $new = json_decode($this->crypto->decrypt($cookie->getValue(),
                $this->config->get('app.security.key'),
                $this->config->get('app.security.salt')), true);

            $this->assertArrayHasKey('d', $new);
            $new = $new['d'];
            $this->assertArrayHasKey('myNamespace', $new);
            $this->assertArrayHasKey('def', $new); // default namespace

            $this->assertEquals('myValue', $new['myNamespace']['myKey']);
            $this->assertEquals('bar', $new['def']['foo']);
        }

        /** @test */
        public function session_is_properly_invalidated()
        {
            $serialized = json_encode(['i' => str_repeat('a', 8), 'd' => ['def' =>  ['foo' => 'bar']]]);
            $rawCookie = $this->crypto->encrypt(
                $serialized,
                $this->config->get('app.security.key'),
                $this->config->get('app.security.salt'));

            $request = Request::create('dogs/logout', 'GET', [], ['SESS' => $rawCookie]);
            $response = $this->app->dispatch($request, true);

            $cookies = $response->headers->getCookies();
            $this->assertEquals(1, count($cookies));

            /** @var Cookie $cookie */
            $cookie = $cookies[0];

            $new = json_decode($this->crypto->decrypt($cookie->getValue(),
                $this->config->get('app.security.key'),
                $this->config->get('app.security.salt')), true);

            $this->assertEquals(['def' => []], $new['d']);
        }

        /** @test */
        public function it_handles_flash_values()
        {
            $serialized = json_encode(['i' => str_repeat('a', 8), 'd' => ['def' => ['foo' => 'bar']]]);
            $rawCookie = $this->crypto->encrypt(
                $serialized,
                $this->config->get('app.security.key'),
                $this->config->get('app.security.salt'));

            $request = Request::create('dogs/flash', 'GET', [], ['SESS' => $rawCookie]);
            $response = $this->app->dispatch($request, true);

            $cookies = $response->headers->getCookies();
            $this->assertEquals(1, count($cookies));

            /** @var Cookie $cookie */
            $cookie = $cookies[0];

            $new = json_decode($this->crypto->decrypt($cookie->getValue(),
                $this->config->get('app.security.key'),
                $this->config->get('app.security.salt')), true);

            $new = $new['d'];
            $this->assertArrayHasKey('def', $new);
            $new = $new['def'];

            $this->assertArrayHasKey('XF_MSG', $new);
            $flash = $new['XF_MSG'];
            $this->assertArrayHasKey('exp', $flash);
            $this->assertArrayHasKey('val', $flash);
            $this->assertEquals(2, count($flash['val']));
            $this->assertEquals(time() + Session::EXPIRE, $flash['exp'], '', 2); // allow a two second delta
        }


        /** @test */
        public function it_deletes_expired_flash_values()
        {
            $serialized = json_encode(['i' => str_repeat('a', 8), 'd' => [ 'def' => [ 'foo' => 'bar', 'XF_someKey' => ['exp' => time() - 10, 'val' => 'someValue']] ]]);
            $rawCookie = $this->crypto->encrypt(
                $serialized,
                $this->config->get('app.security.key'),
                $this->config->get('app.security.salt'));

            $request = Request::create('dogs/bark', 'GET', [], ['SESS' => $rawCookie]);
            $response = $this->app->dispatch($request, true);

            $cookies = $response->headers->getCookies();
            $this->assertEquals(1, count($cookies));

            /** @var Cookie $cookie */
            $cookie = $cookies[0];

            $new = json_decode($this->crypto->decrypt($cookie->getValue(),
                $this->config->get('app.security.key'),
                $this->config->get('app.security.salt')), true);
            $new = $new['d'];

            $this->assertArrayHasKey('def', $new);
            $new = $new['def'];

            $this->assertArrayNotHasKey('XF_someKey', $new);
        }

        /** @test */
        public function it_does_not_delete_not_expired_flash_values()
        {
            $serialized = json_encode(['i' => str_repeat('a', 8), 'd' => ['foo' => 'bar', 'XF_someKey' => ['exp' => time() + 10, 'val' => 'someValue']]]);
            $rawCookie = $this->crypto->encrypt(
                $serialized,
                $this->config->get('app.security.key'),
                $this->config->get('app.security.salt'));

            $request = Request::create('dogs/bark', 'GET', [], ['SESS' => $rawCookie]);
            $response = $this->app->dispatch($request, true);

            $cookies = $response->headers->getCookies();
            $this->assertEquals(1, count($cookies));

            /** @var Cookie $cookie */
            $cookie = $cookies[0];

            $new = json_decode($this->crypto->decrypt($cookie->getValue(),
                $this->config->get('app.security.key'),
                $this->config->get('app.security.salt')), true);
            $new = $new['d'];

            $this->assertArrayHasKey('XF_someKey', $new);
        }

        /** @test */
        public function it_creates_a_new_session_gracefully_when_passed_garbage_data()
        {
            $rawCookie = $this->crypto->encrypt(
                Session::generateId(400),
                Session::generateId(32),
                Session::generateId(32));

            $request = Request::create('dogs/bark', 'GET', [], ['SESS' => $rawCookie]);
            $response = $this->app->dispatch($request, true);

            $cookies = $response->headers->getCookies();
            $this->assertEquals(1, count($cookies));

            /** @var Cookie $cookie */
            $cookie = $cookies[0];

            $new = json_decode($this->crypto->decrypt($cookie->getValue(),
                $this->config->get('app.security.key'),
                $this->config->get('app.security.salt')), true);
            $new = $new['d'];

            $this->assertEquals(['def' => []], $new);
        }
    }
}
