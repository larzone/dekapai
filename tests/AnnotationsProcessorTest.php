<?php namespace Dekapai\Tests;

use Dekapai\AnnotationsProcessor;

class AnnotationsProcessorTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @dataProvider getProcessData
     * @param $annotation
     * @param $expectedResult
     */
    public function testProcess($annotation, $expectedResult)
    {
        $result = AnnotationsProcessor::process($annotation);
        $this->assertEquals($expectedResult, $result);
    }

    public function getProcessData()
    {
        return [
            ['1', '1'],
            ['{"user": "Martin"}', ['user' => 'Martin']],
            [' { "user":  "Martin"   }   ', ['user' => 'Martin']],
            ['{"user": "Martin", "age": 33}   ', ['user' => 'Martin', 'age' => 33]],
            ['{"user": "Martin", "permissions": ["AddUser", "DelUser"]}   ', ['user' => 'Martin', 'permissions' => ['AddUser', 'DelUser']]],
        ];
    }

}
