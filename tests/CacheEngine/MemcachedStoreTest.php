<?php namespace Dekapai\Tests\CacheEngine;

use Dekapai\CacheEngine\MemcachedStore;
use Dekapai\Config\Config;
use Dekapai\Exceptions\KeyNotFoundException;

class MemcachedStoreTest extends \PHPUnit_Framework_TestCase
{

    /**
     * @var Config
     */
    private $config = null;

    /**
     * @var MemcachedStore
     */
    private $memcached = null;

    public function setUp()
    {
        if (is_null($this->config)) {
            $this->config = new Config(['array' => [
                'memcached.server' => 'localhost',
                'memcached.port' => 11211,
            ]]);
        }
        if (is_null($this->memcached)) {
            $this->memcached = new MemcachedStore($this->config);
        }
        $this->memcached->wipe();
    }

    /** @test */
    public function it_has_a_value_that_i_just_set_or_else_it_gets_the_hose_again()
    {
        $this->memcached->set('hellox343', 'hi', 1);
        $this->assertEquals('hi', $this->memcached->get('hellox343'));
    }

    /** @test @expectedException \Dekapai\Exceptions\KeyNotFoundException */
    public function it_doesnt_have_a_value_that_i_didnt_set_or_else_it_gets_the_hose_again()
    {
        $this->memcached->get('hellox658');
    }

    /** @test */
    public function it_can_retrieve_an_array_or_else_it_gets_the_hose_again()
    {
        $testArr = [1,'a',3.14];
        $this->memcached->set('hellox111', $testArr, 1);
        $this->assertEquals($testArr, $this->memcached->get('hellox111'));
    }

    /** @test */
    public function it_doesnt_retrieve_an_item_if_it_is_expired_or_else_it_gets_the_hose_again()
    {
        $this->memcached->set('hellox342', 'kuk', 1);
        $this->assertEquals('kuk', $this->memcached->get('hellox342'));
        $caughtException = false;
        sleep(1);
        try {
            $this->memcached->get('hellox342');
        }
        catch (KeyNotFoundException $e) {
            $caughtException = true;
        }
        $this->assertTrue($caughtException);
    }

    /** @test */
    public function it_wipes_a_key_or_else_it_gets_the_hose_again()
    {
        $this->memcached->set('hellox342', 'kuk', 1);
        $this->assertEquals('kuk', $this->memcached->get('hellox342'));
        $this->memcached->wipe();
        $caughtException = false;
        try {
            $this->memcached->get('hellox342');
        }
        catch (KeyNotFoundException $e) {
            $caughtException = true;
        }
        $this->assertTrue($caughtException);
    }

}
