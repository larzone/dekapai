<?php namespace Dekapai\Tests\ResponseCache;

use Dekapai\CacheEngine\MemcachedStore;
use Dekapai\Interfaces\IKeyValueStore;
use Dekapai\Middleware\CacheMiddleware;
use Dekapai\Config\Config;
use function Dekapai\Hinnyuu\Facade\hinnyuu;
use Dekapai\Oppai;
use Dekapai\Controller;
use Dekapai\Router\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class MockController2 extends Controller
{
    /** @cache 4 */
    public function myMethod($a = 0, $b = 0)
    {
        $response = new JsonResponse(['a' => 'b']);
        $response->headers->set('contenT-securitY-Policy', 'some-policy'); // should be cached by default (case insensitive)
        $response->headers->set('Set-Cookie', 'password=supersecret'); // should not be cached
        return $response;
    }
}

class ResponseCacheTest extends \PHPUnit_Framework_TestCase
{

    private $config;

    public function setUp()
    {
        hinnyuu(\Dekapai\Hinnyuu\Hinnyuu::RESET);
        $this->config = new Config(['array' => [
            'site.subdir' => '',
            'landingpage.subdir' => '',
            'env' => 'test',
            'app.security.key' => 'something',
            'app.security.salt' => 'something completely different',
            'memcached.server' => 'localhost',
            'memcached.port' => 11211,
            'profile.dir' => ''
        ]]);
        hinnyuu([IKeyValueStore::class => MemcachedStore::class]);
    }

    /** @test */
    public function it_caches_responses_with_defined_headers_or_else_it_gets_the_hose_again()
    {
        $routeCollection = function() {
            yield Route::GET(
                'api/a/{a}/b/{b}',
                [MockController2::class, 'myMethod']
            );
        };

        $request = Request::create(
            "/api/a/1/b/2",
            'GET',
            [],
            [],
            [],
            ['DOCUMENT_ROOT' => __DIR__ . '/../']
        );

        $app = new Oppai($routeCollection, $this->config, 'test');

        $app->registerMiddleware(CacheMiddleware::class);

        $response = $app->dispatch( $request, true );
        $this->assertEquals(json_encode(['a' => 'b']), $response->getContent());
        $this->assertEquals('application/json', $response->headers->get('Content-Type'));
        $this->assertTrue($response->headers->has('CONTENT-SECURITY-POLICY'));
        $this->assertEquals('some-policy', $response->headers->get('CONTENT-SECURITY-POLICY'));
        $this->assertTrue($response->headers->has('SET-COOKIE'));  // fine to include it the first time

        // getting the response again (now cached), we should have some headers
        $response = $app->dispatch( $request, true );
        $this->assertEquals(json_encode(['a' => 'b']), $response->getContent());
        $this->assertEquals('application/json', $response->headers->get('Content-Type'));
        $this->assertTrue($response->headers->has('Expires'));
        $this->assertTrue($response->headers->has('Last-Modified'));
        $this->assertTrue($response->headers->has('Etag'));

        $this->assertTrue($response->headers->has('CONTENT-SECURITY-POLICY'));
        $this->assertEquals('some-policy', $response->headers->get('CONTENT-SECURITY-POLICY'));
        $this->assertFalse($response->headers->has('SET-COOKIE')); // not sent again

    }

}
