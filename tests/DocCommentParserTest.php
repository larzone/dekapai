<?php 

class DocCommentParserTest extends PHPUnit_Framework_TestCase
{

    /** @test */
    public function it_parses_a_simple_doc_comment_or_else_it_gets_the_hose_again()
    {
        $returned = \Dekapai\Oppai::parseDocComment("/** @key value */");
        $this->assertEquals(['key' => 'value'], $returned);
    }

    /** @test */
    public function it_parses_a_doc_comment_over_several_lines_or_else_it_gets_the_hose_again()
    {
        $returned = \Dekapai\Oppai::parseDocComment("
          /**
           * @key value
           * @key2 value2
           */
          ");
        $this->assertEquals(['key' => 'value', 'key2' => 'value2'], $returned);
    }

    /** @test */
    public function it_parses_a_doc_comment_with_several_words_or_else_it_gets_the_hose_again()
    {
        $returned = \Dekapai\Oppai::parseDocComment("
          /**
           * @key value
           * @key2 value 2
           */
          ");
        $this->assertEquals(['key' => 'value', 'key2' => 'value 2'], $returned);
    }

    /** @test */
    public function it_returns_an_empty_array_if_the_doc_comment_is_empty_or_else_it_gets_the_hose_again()
    {
        $this->assertEquals([], \Dekapai\Oppai::parseDocComment(""));
        $this->assertEquals([], \Dekapai\Oppai::parseDocComment(null));
    }

    /** @test */
    public function it_parses_a_doc_comment_with_a_json_object_or_else_it_gets_the_hose_again()
    {
        $returned = \Dekapai\Oppai::parseDocComment('
          /**
           * @key{"fugg": "bleh"}
           * @key2{"alright": [1,2,3]}
           */
          ');
        $this->assertEquals(['key' => ['fugg' => 'bleh'], 'key2' => ['alright' => [1,2,3]]], $returned);
    }

    /** @test */
    public function it_parses_a_doc_comment_with_mixed_json_objects_and_strings_or_else_it_gets_the_hose_again()
    {
        $returned = \Dekapai\Oppai::parseDocComment('
          /**
           * @key { "fugg": "bleh"}
           * @key3 12
           * @key2{"alright": [1,2,3] }
           */
          ');
        $this->assertEquals(['key' => ['fugg' => 'bleh'], 'key2' => ['alright' => [1,2,3]], 'key3' => 12], $returned);
    }

    /** @test */
    public function it_parses_a_doc_comment_with_json_over_multiple_lines_or_else_it_gets_the_hose_again()
    {
        $returned = \Dekapai\Oppai::parseDocComment('
          /**
           * @Permission[
           *     {"action": "a", "c": ["e", "f"]},
           *     {"action": "b", "d": ["g"]}
           * ]
           */
          ');
        $this->assertEquals(['Permission' => [["action" => "a", "c" => ["e", "f"]], ["action" => "b", "d" => ["g"]]]], $returned);
    }
}
