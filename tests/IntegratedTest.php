<?php

namespace Dekapai\Tests\FakeDomain {

    use Dekapai\Controller;

    interface ICat
    {
        public function getMeow();
    }

    interface IDog
    {
        public function getBark();
    }

    class Poodle implements IDog
    {
        public function getBark()
        {
            return "woof motherfucker";
        }
    }

    class CatSingleton implements ICat
    {
        private static $instance = null;
        private $random;

        private function __construct()
        {
            $this->random = rand(1, 10);
        }

        public static function getInstance()
        {
            if (is_null(static::$instance)) static::$instance = new static();
            return static::$instance;
        }

        public function getMeow()
        {
            return trim(str_repeat("Meow ", $this->random));
        }
    }

    class AnimalController extends Controller
    {
        public function getBark(IDog $dog)
        {
            return $dog->getBark();
        }

        public function getMeow(ICat $cat)
        {
            return $cat->getMeow();
        }
    }
}

namespace Dekapai\Tests {

    use Dekapai\Config\Config;
    use function Dekapai\Hinnyuu\Facade\hinnyuu;
    use Dekapai\Oppai;
    use Dekapai\Router\Route;
    use Dekapai\Router\RouteCollection;
    use Dekapai\Tests\FakeDomain\AnimalController;
    use Dekapai\Tests\FakeDomain\CatSingleton;
    use Dekapai\Tests\FakeDomain\ICat;
    use Dekapai\Tests\FakeDomain\IDog;
    use Dekapai\Tests\FakeDomain\Poodle;
    use PHPUnit_Framework_TestCase;
    use Symfony\Component\HttpFoundation\Request;

    class IntegratedTest extends PHPUnit_Framework_TestCase
    {

        private $routeCollection;

        /**
         * @var Oppai
         */
        private $app;

        public function setUp()
        {
            hinnyuu(\Dekapai\Hinnyuu\Hinnyuu::RESET);

            $config = new Config(['array' => [
                'site.subdir' => '',
                'landingpage.subdir' => '',
                'env' => 'test',
                'app.security.key' => 'something',
                'profile.dir' => ''
            ]]);

            $this->routeCollection = function() {
                yield Route::GET(
                    'dogs',
                    [AnimalController::class, 'getBark']
                );
                yield Route::GET(
                    'cats',
                    [AnimalController::class, 'getMeow']
                );
            };

            $this->app = new Oppai($this->routeCollection, $config, 'test');

            hinnyuu(['IBogus' => 'Bogus']);
            hinnyuu([IDog::class => Poodle::class]);
            hinnyuu([ICat::class => [CatSingleton::class, 'getInstance']]);
        }

        /** @test */
        public function it_finds_the_service_or_else_it_gets_the_hose_again()
        {
            $request = Request::create(
                "/dogs",
                'GET',
                []
            );

            $response = $this->app->dispatch($request, true);
            $this->assertEquals($response->getContent(), json_encode('woof motherfucker'));
        }

        /** @test */
        public function it_supports_singletons_or_else_it_gets_the_hose_again()
        {
            $request = Request::create(
                "/cats",
                'GET',
                []
            );

            $response = $this->app->dispatch($request, true);
            $result1 = $response->getContent();
            $response = $this->app->dispatch($request, true);
            $result2 = $response->getContent();

            $this->assertEquals($result1, $result2); // singleton should return same number of meows
        }
    }
}
