<?php namespace Tests;

use Dekapai\Interfaces\IRowCountable;
use Dekapai\Validator;

class DbMock implements IRowCountable
{
    private $data = array(
        'users' => array(
            array('id' => '1', 'name' => 'martin'),
            array('id' => '2', 'name' => 'someone'),
        )
    );

    public function getRowCount($table, $column, $testEqual)
    {
        $count = 0;
        foreach ($this->data[$table] as $row) {
            if ($row[$column] == $testEqual) $count++;
        }
        return $count;
    }
}

class ValidatorTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var Validator
     */
    private $validator;

    /**
     * @var DbMock
     */
    private $db;

    public function setUp()
    {
        $this->validator = new Validator();
        $this->db = new DbMock();
        $this->validator->registerPlugin(['unique', 1], "The field %s must be unique.", function($userInput, $parameter) {
            list($table,$column) = explode('.', $parameter);
            if ($this->db->getRowCount($table, $column, $userInput) != 0) {
                return false;
            }
            return true;
        });
        $this->validator->registerPlugin(['exists', 1], "The field %s must refer to an existing row in \1.", function($userInput, $parameter) {
            list($table,$column) = explode('.', $parameter);
            if ($this->db->getRowCount($table, $column, $userInput) == 0) {
                return false;
            }
            return true;
        });
    }

    /** @test */
    public function it_validates_a_valid_email_or_else_it_gets_the_hose_again()
    {
        $this->validator->validate(['email' => ['hej@osas.no', 'email']]);
        $this->assertTrue($this->validator->passes());
    }

    /** @test */
    public function it_invalidates_an_invalid_email_or_else_it_gets_the_hose_again()
    {
        $this->validator->validate(['email' => ['some thing@osas.no', 'email']]);
        $this->assertTrue($this->validator->fails());
    }

    /** @test */
    public function it_finds_required_attributes_or_else_it_gets_the_hose_again()
    {
        $this->validator->validate(['username' => ['', 'required']]);
        $this->assertTrue($this->validator->fails());
    }

    /** @test */
    public function it_finds_minimum_length_or_else_it_gets_the_hose_again()
    {
        $this->validator->validate(['username' => ['hej', 'min(3)']]);
        $this->assertTrue($this->validator->passes());
        $this->validator->validate(['username' => ['hej', 'min(4)']]);
        $this->assertTrue($this->validator->fails());
    }

    /** @test */
    public function it_finds_maximum_length_or_else_it_gets_the_hose_again()
    {
        $this->validator->validate(['username' => ['tjej', 'max(4)']]);
        $this->assertTrue($this->validator->passes());
        $this->validator->validate(['username' => ['tjej', 'max(3)']]);
        $this->assertTrue($this->validator->fails());
    }

    /** @test */
    public function it_detects_matching_attributes_or_else_it_gets_the_hose_again()
    {
        $this->validator->validate(['password' => ['password123', 'min(8)'], 'password_confirm' => ['password123', 'matches(password)']]);
        $this->assertTrue($this->validator->passes());
        $this->validator->validate(['password' => ['password123', 'min(8)'], 'password_confirm' => ['password124', 'matches(password)']]);
        $this->assertTrue($this->validator->fails());
    }

    /** @test */
    public function it_detects_a_unique_string_id_in_a_table_or_else_it_gets_the_hose_again()
    {
        $this->validator->validate(['wow' => ['something', 'unique(users.name)']]);
        $this->assertTrue($this->validator->passes());
        $this->validator->validate(['wow' => ['martin', 'unique(users.name)']]);
        $this->assertTrue($this->validator->fails());
    }

    /** @test */
    public function it_detects_a_unique_numeric_id_in_a_table_or_else_it_gets_the_hose_again()
    {
        $this->validator->validate(['wow' => ['something', 'unique(users.id)']]);
        $this->assertTrue($this->validator->passes());
        $this->validator->validate(['wow' => [1, 'unique(users.id)']]);
        $this->assertTrue($this->validator->fails());
    }

    /** @test */
    public function it_resets_state_after_a_validation_or_else_it_gets_the_hose_again()
    {
        $this->validator->validate(['wow' => ['some value', 'max(5)']]);
        $this->assertTrue($this->validator->fails());

        $this->validator->validate(['wow' => ['some value', 'min(5)']]);
        $this->assertTrue($this->validator->passes());

        $this->validator->validate(['wow' => ['some value', 'max(5)']]);
        $this->assertTrue($this->validator->fails());
    }

    /** @test */
    public function it_detects_an_invalid_date_or_else_it_gets_the_hose_again()
    {
        $this->validator->validate(['somedate' => ['2015-02-30', 'date']]);
        $this->assertTrue($this->validator->fails());
    }

    /** @test */
    public function it_detects_a_valid_date_or_else_it_gets_the_hose_again()
    {
        $this->validator->validate(['somedate' => ['2015-02-28', 'date']]);
        $this->assertTrue($this->validator->passes());
    }

    /** @test */
    public function it_detects_an_invalid_url_or_else_it_gets_the_hose_again()
    {
        $this->validator->validate(['someurl' => ['goatse.cx', 'url']]);
        $this->assertTrue($this->validator->fails());
    }

    /** @test */
    public function it_detects_a_valid_url_or_else_it_gets_the_hose_again()
    {
        $this->validator->validate(['someurl' => ['http://goatse.cx', 'url']]);
        $this->assertTrue($this->validator->passes());
    }

    /** @test */
    public function it_validates_floats_or_else_it_gets_the_hose_again()
    {
        $this->validator->validate(['somenumber' => ['127.9', 'float']]);
        $this->assertTrue($this->validator->passes());
        $this->validator->validate(['somenumber' => ['', 'float']]);
        $this->assertTrue($this->validator->passes());
        $this->validator->validate(['somenumber' => ['', 'float|required']]);
        $this->assertTrue($this->validator->fails());
        $this->validator->validate(['somenumber' => ['127', 'float']]);
        $this->assertTrue($this->validator->passes());
        $this->validator->validate(['somenumber' => ['-127', 'float']]);
        $this->assertTrue($this->validator->passes());
    }

    /** @test */
    public function it_supports_various_scientific_notations_or_else_it_gets_the_hose_again()
    {
        $this->validator->validate(['somenumber' => ['1.e-4', 'float']]);
        $this->assertTrue($this->validator->passes());
        $this->validator->validate(['somenumber' => ['-1.e+4', 'float']]);
        $this->assertTrue($this->validator->passes());
        $this->validator->validate(['somenumber' => ['-1.3434E+004', 'float']]);
        $this->assertTrue($this->validator->passes());
        $this->validator->validate(['somenumber' => ['-134.34E+004', 'float']]);
        $this->assertTrue($this->validator->passes());
        $this->validator->validate(['somenumber' => ['-134.3.4E+004', 'float']]);
        $this->assertTrue($this->validator->fails());
        $this->validator->validate(['somenumber' => ['-134.4E+0.4', 'float']]);
        $this->assertTrue($this->validator->fails());
    }

    /** @test */
    public function it_validates_maximum_values_for_float_numbers_or_else_it_gets_the_hose_again()
    {
        $this->validator->validate(['somenumber' => ['127.9', 'float|max(128)']]);
        $this->assertTrue($this->validator->passes());
        $this->validator->validate(['somenumber' => ['128.0', 'float|max(128)']]);
        $this->assertTrue($this->validator->passes());
        $this->validator->validate(['somenumber' => ['128.1', 'float|max(128)']]);
        $this->assertTrue($this->validator->fails());
    }

    /** @test */
    public function it_validates_minimum_values_for_float_numbers_or_else_it_gets_the_hose_again()
    {
        $this->validator->validate(['somenumber' => ['127.9', 'float|min(128)']]);
        $this->assertTrue($this->validator->fails());
        $this->validator->validate(['somenumber' => ['128.0', 'float|min(128)']]);
        $this->assertTrue($this->validator->passes());
        $this->validator->validate(['somenumber' => ['128.1', 'float|min(128)']]);
        $this->assertTrue($this->validator->passes());
    }


    /** @test */
    public function it_validates_ints_or_else_it_gets_the_hose_again()
    {
        $this->validator->validate(['somenumber' => ['127', 'int']]);
        $this->assertTrue($this->validator->passes());
        $this->validator->validate(['somenumber' => ['', 'int']]);
        $this->assertTrue($this->validator->passes());
        $this->validator->validate(['somenumber' => ['', 'int|required']]);
        $this->assertTrue($this->validator->fails());
        $this->validator->validate(['somenumber' => ['-127', 'int']]);
        $this->assertTrue($this->validator->passes());
        $this->validator->validate(['somenumber' => ['-127.1', 'int']]);
        $this->assertTrue($this->validator->fails());
    }

    /** @test */
    public function it_validates_ages_or_else_it_gets_the_hose_again()
    {
        $date = new \DateTime();
        $interval18years = new \DateInterval('P18Y');
        $interval1day = new \DateInterval('P1D');
        $date->sub($interval18years); // 18 years ago
        $this->validator->validate(['somedate' => [$date->format('Y-m-d'), 'date|minage(18)|maxage(40)']]);
        $this->assertTrue($this->validator->passes());
        $date->add($interval1day); // not yet 18
        $this->validator->validate(['somedate' => [$date->format('Y-m-d'), 'date|minage(18)|maxage(40)']]);
        $this->assertTrue($this->validator->fails());
        $this->validator->validate(['somedate' => ['', 'date|minage(18)|maxage(40)']]); // nothing given, not required
        $this->assertTrue($this->validator->passes());
        $this->validator->validate(['somedate' => ['', 'date|minage(18)|maxage(40)|required']]); // nothing given, but required
        $this->assertTrue($this->validator->fails());
        $this->validator->validate(['somedate' => ['nonsense', 'date|minage(18)|maxage(40)|required']]); // nothing given, but required
        $this->assertTrue($this->validator->fails());
    }

    /** @test */
    public function it_validates_values_in_a_set_or_else_it_gets_the_hose_again()
    {
        $this->validator->validate(['somestring' => ['public', 'required|in(public,private)']]);
        $this->assertTrue($this->validator->passes());
        $this->validator->validate(['somestring' => ['private', 'required|in(public,private)']]);
        $this->assertTrue($this->validator->passes());
        $this->validator->validate(['somestring' => ['somethingelse', 'required|in(public,private)']]);
        $this->assertTrue($this->validator->fails());
    }

    /** @test */
    public function it_validates_values_in_a_set_with_just_one_value_or_else_it_gets_the_hose_again()
    {
        $this->validator->validate(['somestring' => ['public', 'required|in(public)']]);
        $this->assertTrue($this->validator->passes());
        $this->validator->validate(['somestring' => ['private', 'required|in(public)']]);
        $this->assertTrue($this->validator->fails());
    }

    /** @test */
    public function it_validates_profanity_or_else_it_gets_the_hose_again()
    {
        $this->validator->registerPlugin('profanity', 'The field %s contains profanity: \0.', function($input) {
            if ($input == 'damn') {
                return $input;
            }
            return true;
        });
        $this->validator->validate(['somestring' => ['damn', 'required|profanity']]);
        $this->assertTrue($this->validator->fails());
        $error = $this->validator->getErrors()[0];
        $this->assertEquals("The field somestring contains profanity: damn.", $error);
        $this->validator->validate(['somestring' => ['damnit', 'required|profanity']]);
        $this->assertTrue($this->validator->passes());
    }

    /** @test */
    public function it_can_use_a_registered_a_plugin_or_else_it_gets_the_hose_again()
    {
        $this->validator->registerPlugin('custom_password_check', 'The field %s must be a valid password.', function($password) {
            if ($password == 'abc123') {
                return false;
            } else if (strlen($password) < 3) {
                return false;
            } else {
                return true;
            }
        });
        $this->validator->validate(['somepassword' => ['abc123', 'required|custom_password_check']]);
        $this->assertTrue($this->validator->fails());
        $error = $this->validator->getErrors()[0];
        $this->assertEquals("The field somepassword must be a valid password.", $error);
        $this->validator->validate(['somepassword' => ['ab', 'required|custom_password_check']]);
        $this->assertTrue($this->validator->fails());
        $this->validator->validate(['somepassword' => ['thisshouldwork', 'required|custom_password_check']]);
        $this->assertTrue($this->validator->passes());
    }

    /** @test */
    public function it_can_use_a_registered_a_plugin_with_parameters_or_else_it_gets_the_hose_again()
    {
        $this->validator->registerPlugin(['between', 2], 'The field %s must be between \1 and \2 characters long.', function($input, $p1, $p2) {
            if (strlen($input) < $p1) {
                return false;
            } else if (strlen($input) > $p2) {
                return false;
            } else {
                return true;
            }
        });
        $this->validator->validate(['input' => ['123', 'required|between(4,6)']]);
        $this->assertTrue($this->validator->fails());
        $this->validator->validate(['input' => ['1234', 'required|between(4,6)']]);
        $this->assertTrue($this->validator->passes());
        $this->validator->validate(['input' => ['12345', 'required|between(4,6)']]);
        $this->assertTrue($this->validator->passes());
        $this->validator->validate(['input' => ['123456', 'required|between(4,6)']]);
        $this->assertTrue($this->validator->passes());
        $this->validator->validate(['input' => ['1234567', 'required|between(4,6)']]);
        $this->assertTrue($this->validator->fails());
        $error = $this->validator->getErrors()[0];
        $this->assertEquals("The field input must be between 4 and 6 characters long.", $error);
    }

    /** @test */
    public function it_works_even_if_i_forget_percent_s_or_else_it_gets_the_hose_again()
    {
        $this->validator->registerPlugin('not_x', 'invalid', function($input) {
            return $input != 'x';
        });
        $this->validator->validate(['input' => ['x', 'required|not_x']]);
        $this->assertTrue($this->validator->fails());
        $error = $this->validator->getErrors()[0];
        $this->assertEquals('invalid', $error);
    }
}
