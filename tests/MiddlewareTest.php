<?php

namespace DomainLogic {

    use Closure;
    use Dekapai\Middleware\Middleware;
    use Exception;
    use Symfony\Component\HttpFoundation\Request;
    use Symfony\Component\HttpFoundation\Response;

    class CustomException extends Exception {}

    class AnimalController
    {
        public function woof()
        {
            MwObserver::$calledMw[] = 'AnimalController::woof';
            throw new CustomException("test");
        }
        public function meow()
        {
            MwObserver::$calledMw[] = 'AnimalController::meow';
        }
        public function exceptionHandler()
        {
            MwObserver::$calledMw[] = 'AnimalController::exceptionHandler';
            return '';
        }
    }

    class MwObserver
    {
        public static $calledMw = [];
    }

    class MW1 implements Middleware
    {
        public function __invoke(Request $request, Response $response, Closure $next)
        {
            MwObserver::$calledMw[] = 'MW1::before';
            try {
                $response = $next($request, $response);
            }
            finally {
                MwObserver::$calledMw[] = 'MW1::after';
            }
        }
    }

    class MW2 implements Middleware
    {
        public function __invoke(Request $request, Response $response, Closure $next)
        {
            MwObserver::$calledMw[] = 'MW2::before';
            try {
                $response = $next($request, $response);
            }
            finally {
                MwObserver::$calledMw[] = 'MW2::after';
            }
        }
    }

    class MW2fail implements Middleware
    {
        const FAIL = 1;
        public function __invoke(Request $request, Response $response, Closure $next)
        {
            MwObserver::$calledMw[] = 'MW2fail::before';
            if (self::FAIL) throw new CustomException();
            $response = $next($request, $response);
            MwObserver::$calledMw[] = 'MW2fail::after';
            return $response;
        }
    }

    class MW3 implements Middleware
    {
        public function __invoke(Request $request, Response $response, Closure $next)
        {
            MwObserver::$calledMw[] = 'MW3::before';
            try {
                $response = $next($request, $response);
            }
            finally {
                MwObserver::$calledMw[] = 'MW3::after';
            }
        }
    }

}

namespace DekapaiTests {

    use Dekapai\Config\Config;
    use function Dekapai\Hinnyuu\Facade\hinnyuu;
    use Dekapai\Oppai;
    use Dekapai\Router\Route;
    use DomainLogic\AnimalController;
    use DomainLogic\CustomException;
    use DomainLogic\MW1;
    use DomainLogic\MW2;
    use DomainLogic\MW2fail;
    use DomainLogic\MW3;
    use DomainLogic\MwObserver;
    use Exception;
    use PHPUnit_Framework_TestCase;
    use Symfony\Component\HttpFoundation\Request;

    class MiddlewareTest extends PHPUnit_Framework_TestCase
    {
        /**
         * @var Oppai
         */
        private $app;

        public function setUp()
        {
            MwObserver::$calledMw = [];
            hinnyuu(\Dekapai\Hinnyuu\Hinnyuu::RESET);

            $config = new Config(['array' => [
                'site.subdir' => '',
                'landingpage.subdir' => '',
                'env' => 'test',
                'app.security.key' => 'something',
                'profile.dir' => ''
            ]]);

            $routeCollection = function() {
                yield Route::NAMESPACE('animal', function() {
                    yield Route::GET(
                        'dog',
                        [AnimalController::class, 'woof'],
                        [
                            ['class' => MW1::class, 'priority' => 10],
                            ['class' => MW2::class, 'priority' => 5]
                        ]
                    );
                    yield Route::GET(
                        'cat',
                        [AnimalController::class, 'meow'],
                        [
                            ['class' => MW1::class, 'priority' => 10],
                            ['class' => MW2fail::class, 'priority' => 5]
                        ]
                    );
                }, [
                    ['class' => MW3::class, 'priority' => 1]
                ]);
            };

            $this->app = new Oppai($routeCollection, $config, 'test');
        }

        /** @test */
        public function it_calls_middleware_after_the_controller_after_an_exception_in_the_controller_or_else_it_gets_the_hose_again()
        {
            $this->app->registerExceptionHandler(CustomException::class, function(Exception $e) {
                return [AnimalController::class, [], 'exceptionHandler'];
            });

            $request = Request::create(
                "animal/dog",
                'GET',
                []
            );

            $this->app->dispatch($request, true);
            $expectedOrder = [
                'MW1::before',
                'MW2::before',
                'MW3::before',
                'AnimalController::woof',
                'MW3::after',
                'MW2::after',
                'MW1::after',
                'AnimalController::exceptionHandler',
            ];
            $this->assertCount(count($expectedOrder), MwObserver::$calledMw);
            for ($i = 0; $i < count($expectedOrder); $i++) {
                $this->assertEquals($expectedOrder[$i], MwObserver::$calledMw[$i]);
            }
        }

        /** @test */
        public function it_calls_middleware_after_the_controller_after_an_exception_in_the_middleware_or_else_it_gets_the_hose_again()
        {
            $this->app->registerExceptionHandler(CustomException::class, function(Exception $e) {
                return [AnimalController::class, [], 'exceptionHandler'];
            });

            $request = Request::create(
                "animal/cat",
                'GET',
                []
            );

            $this->app->dispatch($request, true);
            $expectedOrder = [
                'MW1::before',
                'MW2fail::before',
                'MW1::after',
                'AnimalController::exceptionHandler',
            ];
            $this->assertCount(count($expectedOrder), MwObserver::$calledMw);
            for ($i = 0; $i < count($expectedOrder); $i++) {
                $this->assertEquals($expectedOrder[$i], MwObserver::$calledMw[$i]);
            }
        }
    }
}
