<?php namespace Dekapai\Tests;

use Dekapai\CacheEngine\MemcachedStore;
use Dekapai\Config\Config;
use Dekapai\Exceptions\KeyNotFoundException;
use Dekapai\Middleware\CacheMiddleware;
use Dekapai\ResponseCache\ResponseCache;
use Exception;
use PHPUnit_Framework_MockObject_MockObject;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class CacheMiddlewareTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var PHPUnit_Framework_MockObject_MockObject
     */
    private $cache;

    public function setUp()
    {
        $config = new Config(['array' => [
            'app.security.salt' => 'whatever dude',
            'memcached.server' => 'localhost',
            'memcached.port' => 11211
        ]]);
        $memc = new MemcachedStore($config);
        $this->cache = $this->getMockBuilder(ResponseCache::class)->setConstructorArgs([$memc, $config])->getMock();
    }

    public function testBefore()
    {
        $this->cache->expects($this->once())->method('getCachedResponse')->willReturn('something');

        $mw = new CacheMiddleware($this->cache, 3600);
        $request = Request::create('/this/is/a/test');
        $response = new Response();
        $response = $mw($request, $response, function() { throw new Exception("Should not be called."); });
        $this->assertEquals('something', $response);
    }

    public function testAfter()
    {
        $response = new Response();
        $response->headers->set('Content-Type', 'image/png');

        $this->cache->expects($this->once())->method('getCachedResponse')->willThrowException(new KeyNotFoundException());
        $this->cache->expects($this->once())->method('saveResponse');

        $mw = new CacheMiddleware($this->cache, 3600);
        $request = Request::create('/this/is/a/test');
        $response = $mw($request, $response, function($request, $response) { return $response; });

        $this->assertEquals('image/png', $response->headers->get('Content-Type'));
    }

}
