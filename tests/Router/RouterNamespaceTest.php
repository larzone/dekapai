<?php namespace Router;

use Dekapai\Router\Route;
use Dekapai\Router\Router;
use PHPUnit_Framework_TestCase;

class RouterNamespaceTest extends PHPUnit_Framework_TestCase
{

    /** @test */
    public function it_finds_a_route_within_a_namespace_or_else_it_gets_the_hose_again()
    {
        $routeCollection = function() {
            yield Route::NAMESPACE('q', function() {
                yield Route::POST('a', ['Cv', 'Mv'], ['MWxy', 'MWxz']);
                yield Route::NAMESPACE('b', function () {
                    yield Route::POST('a', ['C1', 'M1'], [['class' => 'MWr', 'priority' => 4]]);
                    yield Route::POST('b', ['C1', 'M1']);
                }, ['MWeb']);
            }, ['MWX']);
        };

        $router = new Router($routeCollection, 'q/b/a', 'POST');
        $route = $router->resolveRoute();
        $this->assertEquals('q/b/a', $route->getUri());
        $this->assertEquals([['class' => 'MWr', 'priority' => 4], 'MWX', 'MWeb'], $route->getRouteMiddleware());
    }

    /** @test */
    public function it_finds_a_route_within_a_nameless_namespace_or_else_it_gets_the_hose_again()
    {
        $routeCollection = function() {
            yield Route::NAMESPACE('', function() {
                yield Route::POST('a', ['Cv', 'Mv'], ['MWxy', 'MWxz']);
                yield Route::NAMESPACE('b', function () {
                    yield Route::POST('a', ['C1', 'M1'], [['class' => 'MWr', 'priority' => 4]]);
                    yield Route::POST('b', ['C1', 'M1']);
                }, ['MWeb']);
            }, ['MWX']);
        };

        $router = new Router($routeCollection, 'b/a', 'POST');
        $route = $router->resolveRoute();
        $this->assertEquals('b/a', $route->getUri());
    }

    /** @test */
    public function it_does_stuff_it_should_do()
    {
        $routeCollection = function() {
            yield Route::NAMESPACE('', function() {
                yield Route::POST('test/shit', ['C1', 'M1'], []);
                yield Route::NAMESPACE('apx', function () {
                    yield Route::POST('test/bajs', ['C1', 'M1'], []);
                    yield Route::POST('test/shit', ['C1', 'M1'], []);
                }, []);
                yield Route::NAMESPACE('api', function () {
                    yield Route::POST('test/bajs', ['C1', 'M1'], []);
                    yield Route::POST('test/shit', ['C1', 'M1'], []);
                }, []);
            }, []);
        };

        $router = new Router($routeCollection, 'api/test/shit', 'POST');
        $route = $router->resolveRoute();
        $this->assertEquals('api/test/shit', $route->getUri());
    }
}
