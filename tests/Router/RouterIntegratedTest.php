<?php

namespace DomainY {

    class Post
    {
        private $id;
        public function __construct($id)
        {
            $this->id = $id;
        }
        public function getId()
        {
            return $this->id;
        }
    }

    class PostRepository
    {
        private $instantiated = false;
        private $posts = null;
        public function __construct()
        {
            $this->instantiated = true;
            $this->posts = [];
        }
        public function isInstantiated()
        {
            return $this->instantiated;
        }
        public function savePost(Post $post)
        {
            $this->posts[] = $post;
        }
    }
    class PostsController
    {
        private $instantiated = false;
        private $postRepository;
        public function __construct(PostRepository $postRepository)
        {
            $this->instantiated = true;
            $this->postRepository = $postRepository;
        }
        public function showPost(Post $post)
        {
            if (!$this->instantiated) {
                throw new \Exception("Posts controller is not instantiated.");
            }
            if (!$this->postRepository->isInstantiated()) {
                throw new \Exception("Posts repository is not instantiated.");
            }
            $this->postRepository->savePost($post);
            return $post->getId();
        }
    }
}

namespace Tests\Router {

    use Dekapai\Config\Config;
    use Dekapai\Oppai;
    use Dekapai\Router\Route;
    use Dekapai\Router\RouteCollection;
    use DomainY\PostsController;
    use PHPUnit_Framework_TestCase;
    use Symfony\Component\HttpFoundation\Request;

    class RouterIntegratedTest extends PHPUnit_Framework_TestCase
    {

        /**
         * @var Config
         */
        private $config;

        public function setUp()
        {
            $this->config = new Config(['array' => [
                'site.subdir' => 'mysite',
                'landingpage.subdir' => '',
                'env' => 'dev',
                'app.security.key' => 'something',
                'profile.dir' => ''
            ]]);
        }

        /** @test */
        public function test_case_1()
        {
            $routeCollection = function() {
                yield Route::GET(
                    'getpost/{DomainY\Post@id|int}',
                    [PostsController::class, 'showPost']
                );
            };

            $request = Request::create(
                "/mysite/getpost/174",
                'GET'
            );

            $app = new Oppai( $routeCollection, $this->config, 'test' );
            $response = $app->dispatch( $request, true );
            $this->assertEquals(174, $response->getContent());

        }
    }
}